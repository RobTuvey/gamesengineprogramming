uniform sampler2D in_Texture;

varying vec2 var_Uv;
varying vec3 var_LightPos;
varying vec3 var_Position;
varying vec3 var_Normal;
varying vec4 var_Colour;
varying vec3 var_CameraPos;
varying float var_SpecInt;
varying float var_DiffInt;

void main()
{
vec3 lightDirection = var_LightPos - var_Position;

float diffuseFactor = max(0.0, dot(normalize(var_Normal), normalize(lightDirection)));
vec4 diffuseColour = vec4(vec3(1, 1, 1) * diffuseFactor * var_DiffInt, 1);

vec3 reflection = normalize(reflect(-normalize(lightDirection), normalize(var_Normal)));
float specularFactor = max(0.0, dot(normalize(var_Normal), reflection));
vec4 specularColour = vec4(1.0, 1.0, 1.0, 1.0) * pow(specularFactor * var_SpecInt, 32);

  gl_FragColor = texture2D(in_Texture, var_Uv) + diffuseColour + specularColour;
}
