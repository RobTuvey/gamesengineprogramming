uniform sampler2D in_Texture;

varying vec2 var_Uv;

void main()
{
  gl_FragColor = texture2D(in_Texture, var_Uv);
  gl_FragColor.w = 1;
}
