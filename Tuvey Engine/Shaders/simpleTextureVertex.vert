uniform mat4 in_Projection;
uniform mat4 in_View;
uniform mat4 in_Model;
uniform mat4 in_NormalMatrix;
uniform vec3 in_Light;
uniform vec4 in_Colour;
uniform vec3 in_CameraPos;
uniform float in_SpecInt;
uniform float in_DiffInt;

attribute vec3 in_Position;
attribute vec2 in_Uv;
attribute vec3 in_Normal;

varying vec2 var_Uv;
varying vec3 var_LightPos;
varying vec3 var_Position;
varying vec3 var_Normal;
varying vec4 var_Colour;
varying vec3 var_CameraPos;
varying float var_SpecInt;
varying float var_DiffInt;

void main()
{
var_Normal = normalize(in_NormalMatrix * vec4(in_Normal, 1));
var_Position = vec3(in_View * in_Model * vec4(in_Position, 1));
var_Uv = in_Uv;
var_LightPos = vec4(in_View * vec4(in_Light, 1)).xyz;
var_Colour = in_Colour;
var_CameraPos = in_CameraPos;

var_SpecInt = in_SpecInt;
var_DiffInt = in_DiffInt;

gl_Position = in_Projection * in_View * in_Model * vec4(in_Position, 1);
}
