uniform float in_SpecInt;
uniform float in_DiffInt;

varying vec2 var_Uv;
varying vec3 var_LightPos;
varying vec3 var_Position;
varying vec3 var_Normal;
varying vec4 var_Colour;
varying float var_SpecInt;
varying float var_DiffInt;

void main()
{
  vec3 lightDirection = var_LightPos - var_Position;
  float lightDistance = length(var_LightPos - var_Position);
  float lightPower = 5.0f;

  float diffuseFactor = max(dot(normalize(var_Normal), normalize(lightDirection)), 0.5);
  diffuseFactor = diffuseFactor * (1.0 / (1.0 + (0.05 * lightDistance * lightDistance)));
  diffuseFactor = diffuseFactor * lightPower;
  vec4 diffuseColour = vec4(vec3(1, 1, 1) * diffuseFactor * var_DiffInt, 1);

  vec3 reflection = normalize(reflect(-normalize(lightDirection), normalize(var_Normal)));
  float specularFactor = max(dot(normalize(lightDirection), reflection), 0.0);
  vec4 specularColour = vec4(1.0, 1.0, 1.0, 1.0) * pow(specularFactor, 16);
  specularColour = specularColour * var_SpecInt;
  gl_FragColor = (var_Colour * diffuseFactor) + specularColour;
  gl_FragColor.w = 1.0;
}
