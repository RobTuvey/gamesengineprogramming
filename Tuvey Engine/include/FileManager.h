#pragma once

#include <string>

namespace tuvey
{
	namespace engine
	{
		class Mesh;

		class FileManager
		{
		public:
			static Mesh* loadObj(std::string filename);
			static std::string loadFile(std::string filename);
		};
	}
}