#pragma once

#include "Component.h"
#include "Mesh.h"

namespace tuvey
{
	namespace engine
	{
		class MeshComponent : public Component
		{
		public:
			MeshComponent();
			~MeshComponent();

			void destroy();

			shared<Mesh> getMesh();
			void setMesh(shared<Mesh> mesh);

		private:
			shared<Mesh>		mesh_;
		};
	}
}