#pragma once

#include "Common.h"
#include "Component.h"
#include <glm.hpp>

namespace tuvey
{
	namespace engine
	{
		class Mesh;
		class Shader;
		class Material;

		class MeshRenderer : public Component
		{
		public:
			MeshRenderer();
			MeshRenderer(shared<Material> material);
			~MeshRenderer();

			void start();
			void awake();
			void render();
			void destroy();

			glm::vec3 getAmbientColour();
			shared<Material> getMaterial();

			void setAmbientColour(glm::vec3 colour);
			void setMaterial(shared<Material> material);

		private:
			weak<Mesh>				mesh_;
			shared<Material>		material_;

			glm::vec3				ambientColour_;
		};
	}
}