#pragma once

#include "Component.h"
#include "Bounds.h"

namespace tuvey
{
	namespace engine
	{
		class Collision;

		class Collider : public Component
		{
		public:
			Collider();
			~Collider();

			void awake();
			void update();

			Bounds getBounds();

			glm::vec3 getCollisionNormal();

			glm::mat3 getInvInertiaTensor();

			bool checkCollision(weak<Collider> other);

		protected:
			void updateBounds();

			Bounds					bounds_;

			shared<Collision>		latestCollision_;

			glm::mat3				invInertiaTensor_;
		};
	}
}