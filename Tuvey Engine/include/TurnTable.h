#pragma once

#include "Component.h"

namespace tuvey
{
	namespace engine
	{
		class Transform;

		class TurnTable : public Component
		{
		public:
			TurnTable();
			~TurnTable();

			void update();
			void awake();

			weak<Transform>			transform_;
		};
	}
}