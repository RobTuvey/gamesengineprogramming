#pragma once

#include <chrono>

namespace tuvey
{
	namespace engine
	{
		using namespace std::chrono;

		class Application;

		class Time
		{
			friend class Application;

		public:
			static void updateDeltaTime();

			static double getDeltaTime();
			static double getSinceLastDisplay();

			static void resetSinceLastDisplay();

		private:
			static duration<double>							deltaTime_;
			static time_point<high_resolution_clock>		lastTime_;
			static double									sinceLastDisplay_;
		};
	}
}