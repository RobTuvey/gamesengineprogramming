#pragma once

#include "Common.h"

namespace tuvey
{
	namespace engine
	{
		class GameObject;

		class Plane
		{
		public:
			static GameObject* create();

		private:
			static int		planeNo;
		};
	}
}