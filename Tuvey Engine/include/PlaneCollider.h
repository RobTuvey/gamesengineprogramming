#pragma once

#include <glm.hpp>

#include "Collider.h"

namespace tuvey
{
	namespace engine
	{
		class PlaneCollider : public Collider
		{
		public:
			PlaneCollider();
			~PlaneCollider();

			void awake();

			glm::vec3 getNormal();

		private:
			glm::vec3		normal_;
		};
	}
}