#pragma once

#include <glm.hpp>
#include <GL/glew.h>
#include <string>
#include <vector>

#include "Common.h"

namespace tuvey
{
	namespace engine
	{
		class Shader;

		class Material : public std::enable_shared_from_this<Material>
		{
		public:
			Material();
			Material(std::string vertexFile, std::string fragmentFile);
			~Material();

			void refreshAttributeIds();
			void refreshUniformIds();
			void insertData();
			void loadTexture(std::string filename);

			void setMatrix(std::string name, glm::mat4 value);
			void setFloat(std::string name, float value);
			void setVector2(std::string name, glm::vec2 value);
			void setVector3(std::string name, glm::vec3 value);
			void setVector4(std::string name, glm::vec4 value);
			void setSampler(std::string, int value);

			glm::mat4 getMatrix(std::string name);
			float getFloat(std::string name);
			glm::vec2 getVector2(std::string name);
			glm::vec3 getVector3(std::string name);
			glm::vec4 getVector4(std::string name);
			int getSampler(std::string name);

			GLint getPositionId();
			GLint getUvId();
			GLint getNormalId();
			GLint getModelUId();
			GLint getTextureId();

			shared<Shader> getShader();
			
		private:
			std::vector<glm::mat4>			matrices_;
			std::vector<float>				floats_;
			std::vector<glm::vec2>			vector2s_;
			std::vector<glm::vec3>			vector3s_;
			std::vector<glm::vec4>			vector4s_;
			std::vector<int>				samplers_;

			std::vector<int>				matrixIds_;
			std::vector<int>				floatIds_;
			std::vector<int>				vector2Ids_;
			std::vector<int>				vector3Ids_;
			std::vector<int>				vector4Ids_;
			std::vector<int>				samplerIds_;

			std::vector<std::string>		matrixNames_;
			std::vector<std::string>		floatNames_;
			std::vector<std::string>		vector2Names_;
			std::vector<std::string>		vector3Names_;
			std::vector<std::string>		vector4Names_;
			std::vector<std::string>		samplerNames_;

			shared<Shader>					shader_;

			GLuint							positionId_;
			GLuint							uvId_;
			GLuint							normalId_;
			GLuint							modelUniformId_;
			GLuint							textureId_;

			float							specularIntensity_;
			float							diffuseIntensity_;

			bool							dirtyIds_;
		};
	}
}