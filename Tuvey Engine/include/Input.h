#pragma once

#include <map>
#include <vector>
#include "KeyCodes.h"
#include "SDL.h"

namespace tuvey
{
	namespace engine
	{
		class Application;

		class Input
		{
			friend class tuvey::engine::Application;
			
		public:
			static bool getKey(int keyCode);
			static bool getKeyDown(int keyCode);
			static bool getKeyUp(int keyCode);

			static int getMouseX();
			static int getMouseY();
			static bool getLeftClick();
			static bool getRightClick();

			static bool getButton(int buttonId);
			static bool getButtonDown(int buttonId);
			static bool getButtonUp(int buttonId);
			static int getAxis(int axisId);

			static SDL_GameController* getController(int deviceId);

			static void resetUpAndDownKeys();
			
			static void getInput();

			static void addController(int id);
			static void removeController(int id);

		private:
			static std::vector<int>						keys_;
			static std::vector<int>						keysDown_;
			static std::vector<int>						keysUp_;

			static int									mouseX_;
			static int									mouseY_;
			static bool									leftClick_;
			static bool									rightClick_;

			static std::vector<int>						buttons_;
			static std::vector<int>						buttonsDown_;
			static std::vector<int>						buttonsUp_;
			static std::map<int, int>					axis_;

			static std::map<int, SDL_GameController*>	gameControllers_;
		};
	}
}