#pragma once

#include "Collider.h"

namespace tuvey
{
	namespace engine
	{
		class BoxCollider;
		class PlaneCollider;

		class SphereCollider : public Collider, public std::enable_shared_from_this<SphereCollider>
		{
		public:
			SphereCollider();
			~SphereCollider();

			void awake();
			void update();

			glm::vec3 getCentre();
			float getRadius();
			Bounds getBounds();
			
			bool intersectsPlane(weak<PlaneCollider> other);
			bool intersectsBox(weak<BoxCollider> other);
			bool intersectsSphere(weak<SphereCollider> other);
			bool checkCollision(weak<Collider> other);
		private:
			float		radius_;

		};
	}
}