#pragma once

#include <gtc/matrix_transform.hpp>

#include "Component.h"

namespace tuvey
{
	namespace engine
	{
		class Transform;

		class Camera : public Component, public std::enable_shared_from_this<Camera>
		{
		public:
			Camera();
			~Camera();

			glm::mat4 getProjectionMatrix();
			void setProjectionMatrix(glm::mat4 matrix);

			static weak<Camera> getCurrent();

			void awake();

		private:
			glm::mat4		projectionMatrix_;
		};
	}
}