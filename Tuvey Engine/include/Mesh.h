#pragma once

#include <GL/glew.h>
#include <vector>

#include "Bounds.h"
#include "Common.h"
#include "glm.hpp"

namespace tuvey
{
	namespace engine
	{
		class Mesh
		{
		public:
			Mesh();
			Mesh(std::vector<glm::vec3> vertices, std::vector<glm::vec2> uvs, std::vector<glm::vec3> normals);
			~Mesh();

			void initializeBuffers();

			void setVertices(std::vector<glm::vec3> vertices);
			void setUVs(std::vector<glm::vec2> uvs);
			void setNormals(std::vector<glm::vec3> normals);

			std::vector<glm::vec3>& getVertices();
			std::vector<glm::vec2>& getUVs();
			std::vector<glm::vec3>& getNormals();

			GLuint getVertexBuffer();
			GLuint getUvBuffer();
			GLuint getNormalBuffer();
			
			void recalculateBounds();

			Bounds getBounds();

		private:
			std::vector<glm::vec3>		vertices_;
			std::vector<glm::vec2>		uvs_;
			std::vector<glm::vec3>		normals_;

			GLuint						vertexBuffer_;
			GLuint						uvBuffer_;
			GLuint						normalBuffer_;

			Bounds						bounds_;

		};
	}
}