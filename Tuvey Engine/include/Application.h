#pragma once

#include <GL/glew.h>
#include <string>
#include <vector>

#include "Common.h"
#include "SDL.h"

namespace tuvey
{
	namespace engine
	{
		class Camera;
		class GameObject;
		class Input;
		class Material;
		class Screen;

		struct Context
		{
			bool started;
			bool quit;

			std::vector<shared<GameObject>> gameObjects;
			
			std::vector<shared<Camera>> cameras;
			weak<Camera> currentCamera;

			weak<Material> currentMaterial;

			SDL_Window* mainWindow;
			SDL_Renderer* mainRenderer;
			SDL_GLContext glContext;
		};

		class Application
		{
			friend class tuvey::engine::Camera;
			friend class tuvey::engine::GameObject;
			friend class tuvey::engine::Input;
			friend class tuvey::engine::Material;
			friend class tuvey::engine::Screen;

		public:
			static void init();
			static void run();
			static void update();
			static void render();
			static void destroy();

		private:
			static bool initOpenGl();
			static bool initSdl();

			static shared<Context>		context_;

			static float				idealFrameTime_;
		};
	}
}