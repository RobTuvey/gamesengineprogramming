#pragma once

#include <string>
#include <GL/glew.h>

namespace tuvey
{
	namespace engine
	{
		class Shader
		{
		public:
			Shader();
			Shader(std::string vertexFile, std::string fragmentFile);
			~Shader();

			void loadVertexShader(std::string vertexFile);
			void loadFragmentShader(std::string fragmentFile);

			GLuint getProgramId();
			GLuint getVertexShaderId();
			GLuint getFragmentShaderId();

		private:
			void createProgram();
			bool checkShaderCompiled(GLuint shader);

			std::string			vertexShader_;
			std::string			fragmentShader_;

			GLuint				programId_;
			GLuint				vertexShaderId_;
			GLuint				fragmentShaderId_;
		};
	}
}