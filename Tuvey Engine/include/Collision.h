#pragma once

#include <glm.hpp>

#include "Collider.h"
#include "Common.h"

namespace tuvey
{
	namespace engine
	{
		class Collision
		{
		public:
			Collision();
			Collision(weak<Collider> A, weak<Collider>B, glm::vec3 normal);
			~Collision();

			glm::vec3 getNormal();

			weak<Collider> getColliderA();
			weak<Collider> getColliderB();

		private:
			glm::vec3			normal_;

			weak<Collider>		colliderA_;
			weak<Collider>		colliderB_;
		};
	}
}