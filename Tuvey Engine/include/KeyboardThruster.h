#pragma once

#include "Component.h"

namespace tuvey
{
	namespace engine
	{
		class RigidBody;

		class KeyboardThruster : public Component
		{
		public:
			KeyboardThruster();
			~KeyboardThruster();

			void awake();
			void update();

		private:
			float					force_;
			float					torque_;

			weak<RigidBody>			rigidBody_;

		};
	}
}