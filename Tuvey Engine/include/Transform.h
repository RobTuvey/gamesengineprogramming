#pragma once

#include <glm.hpp>
#include <vector>

#include "Common.h"
#include "Component.h"

namespace tuvey
{
	namespace engine
	{
		class Transform : public Component, public std::enable_shared_from_this<Transform>
		{
		public:
			Transform();
			~Transform();

			void setParent(weak<Transform> parent);

			glm::vec3 getLocalPosition();
			glm::vec3 getLocalRotation();
			glm::vec3 getLocalScale();

			glm::vec3 getPosition();
			glm::vec3 getRotation();
			glm::vec3 getScale();

			void setLocalPosition(glm::vec3 vector);
			void setLocalRotation(glm::vec3 vector);
			void setLocalScale(glm::vec3 vector);
			
			void setPosition(glm::vec3 vector);
			void setRotation(glm::vec3 vector);
			
			void translate(glm::vec3 vector);
			void rotate(glm::vec3 vector);

		private:
			glm::vec3							localPosition_;
			glm::vec3							localRotation_;
			glm::vec3							localScale_;

			weak<Transform>						parent_;
			std::vector<weak<Transform>>		children_;
		};
	}
}