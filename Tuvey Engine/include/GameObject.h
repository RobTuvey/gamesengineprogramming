#pragma once

#include <vector>

#include "Application.h"
#include "Component.h"
#include "Object.h"
#include "Transform.h"
#include "Camera.h"

namespace tuvey
{
	namespace engine
	{
		class Application;
		class Transform;

		class GameObject : public Object, public std::enable_shared_from_this<GameObject>
		{
			friend class Application;
			friend class BoxCollider;
			friend class SphereCollider;
		
		public:
			GameObject();
			GameObject(std::string name);
			~GameObject();

			std::string getName();

			template <typename C>
			shared<C> addComponent()
			{
				shared<C> component(new C());
				components_.push_back(component);

				component->gameObject = shared_from_this();
				if (Application::context_->started) component->awake();

				return component;
			}

			template <typename C>
			shared<C> getComponent()
			{
				for (size_t i = 0; i < components_.size(); i++)
				{
					shared<C> component = std::dynamic_pointer_cast<C>(components_.at(i));
					if (component != NULL)
						return component;
				}

				return NULL;
			}

			template <typename C>
			static std::vector<weak<C>> findComponentsOfType()
			{
				std::vector<weak<C>> components;

				for (size_t j = 0; j < Application::context_->gameObjects.size(); j++)
				{
					weak<GameObject> gameObject = Application::context_->gameObjects.at(j);
					for (size_t i = 0; i < gameObject.lock()->components_.size(); i++)
					{
						shared<C> component = std::dynamic_pointer_cast<C>(gameObject.lock()->components_.at(i));
						if (component != NULL)
							components.push_back(component);
					}
				}

				return components;
			}

		private:
			std::vector<shared<Component>>		components_;

			std::string							name_;

			virtual void start();
			virtual void awake();
			virtual void update();
			virtual void destroy();
			virtual void render();
			virtual void onCollision(weak<Collider> other);
		};
	}
}