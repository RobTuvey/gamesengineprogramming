#pragma once

#include <iostream>

#include "Common.h"
#include "Object.h"

namespace tuvey
{
	namespace engine
	{
		class Collider;
		class GameObject;
		class Transform;

		class Component : public Object
		{
			friend class GameObject;
			
		public:
			virtual void start();
			virtual void awake();
			virtual void update();
			virtual void render();
			virtual void destroy();
			virtual void onCollision(weak<Collider> other);

			weak<Transform> getTransform();

			weak<GameObject>		gameObject;
		};
	}
}