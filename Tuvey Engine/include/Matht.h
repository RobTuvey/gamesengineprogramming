#pragma once

#include <glm.hpp>
#include <gtc/matrix_transform.hpp>

namespace tuvey
{
	namespace engine
	{
		class Math
		{
		public:
			static float deg2Rad(float degree);

			static glm::mat4 getTransformation(glm::vec3 position, glm::vec3 rotation, glm::vec3 sale);
			static glm::mat3 getRotationMatrix(glm::vec3 rotation);

			static double magnitude(glm::vec3 vector);
			static double shortestAngle(glm::vec3 a, glm::vec3 b);
			static float antiClockwiseAngle(glm::vec3 a, glm::vec3 b, glm::vec3 c);
			static float clockwiseAngle(glm::vec3 a, glm::vec3 b, glm::vec3 c);
			static float triple(glm::vec3 a, glm::vec3 b, glm::vec3 c);

			static float		PI;
		};
	}
}