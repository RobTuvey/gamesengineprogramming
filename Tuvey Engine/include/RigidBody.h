#pragma once

#include <glm.hpp>

#include "Component.h"
#include "Bounds.h"

namespace tuvey
{
	namespace engine
	{
		class Collider;
		class Bounds;

		class RigidBody : public Component
		{
		public:
			RigidBody();
			~RigidBody();

			void awake();
			void update();
			void onCollision(weak<Collider> other);

			void adjustCollisionPosition(weak<Collider> other);

			float getMass();

			void setGravity(float gravity);
			void setDrag(float drag);
			void setAngularDrag(float angularDrag);
			void setMass(float mass);
			void setUsesGravity(bool usesGravity);

			void addForce(glm::vec3 force);
			void addTorque(glm::vec3 torque);

			float computeImpulseMagnitude(glm::vec3 relativeVelocity, glm::vec3 collisionNormal, weak<RigidBody> other);
			glm::mat3 getInertiaTensor();
			glm::vec3 getCollisionDistance(glm::vec3 collisionNorml);

		private:
			float					gravity_;
			float					gravityComponent_;
			float					drag_;
			float					angularDrag_;
			float					mass_;
			float					scale_;
			float					elasticity_;
			float					linearImpulseDampener_;
			float					angularImpulseDampener_;

			glm::vec3				force_;
			glm::vec3				torque_;
			glm::vec3				linearAcceleration_;
			glm::vec3				angularAcceleration_;
			glm::vec3				linearVelocity_;
			glm::vec3				angularVelocity_;

			bool					usesGravity_;
			bool					isKinematic_;

			weak<Transform>			transform_;
			Bounds					bounds_;
		};
	}
}