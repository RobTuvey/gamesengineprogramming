#pragma once

namespace tuvey
{
	namespace engine
	{
		class Screen
		{
			friend class Application;

		public:
			static int getWidth();
			static int getHeight();

		private:
			static int			width;
			static int			height;
		};
	}
}