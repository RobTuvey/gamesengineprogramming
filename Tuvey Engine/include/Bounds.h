#pragma once

#include <glm.hpp>

namespace tuvey
{
	namespace engine
	{
		class Bounds
		{
		public:
			Bounds();
			Bounds(glm::vec3 centre, glm::vec3 size);
			~Bounds();

			glm::vec3 getCentre();
			glm::vec3 getExtent();
			glm::vec3 getSize();
			glm::vec3 getMin();
			glm::vec3 getMax();

			void setMinMax(glm::vec3 min, glm::vec3 max);
			void setCentre(glm::vec3 centre);
		
		private:
			glm::vec3		centre_;
			glm::vec3		extent_;
			glm::vec3		size_;
			glm::vec3		min_;
			glm::vec3		max_;
		};
	}
}