#pragma once

#include <vector>

#include "Collider.h"

namespace tuvey
{
	namespace engine
	{
		class SphereCollider;
		class PlaneCollider;

		class BoxCollider : public Collider, public std::enable_shared_from_this<BoxCollider>
		{
		public:
			BoxCollider();
			~BoxCollider();

			void awake();
			void update();

			glm::vec3 getCentre();
			glm::vec3 getNormal(glm::vec3 point, Bounds otherBounds, glm::vec3 otherPosition);
			Bounds getBounds();

			bool intersectsBox(weak<BoxCollider> other);
			bool intersectsSphere(weak<SphereCollider> other);
			bool intersectsPlane(weak<PlaneCollider> other);
			bool checkCollision(weak<Collider> other);
		private:
			std::vector<glm::vec3> translateToWorld(std::vector<glm::vec3> vertices);

			std::vector<glm::vec3>		vertices_;
		};
	}
}