#include "KeyboardThruster.h"
#include "GameObject.h"
#include "Input.h"
#include "Matht.h"
#include "Time.h"
#include "RigidBody.h"

namespace tuvey
{
	namespace engine
	{
		KeyboardThruster::KeyboardThruster()
		{
			std::cout << "++ [KeyboardThruster] Component Added" << std::endl;
		}

		KeyboardThruster::~KeyboardThruster()
		{
			std::cout << "-- [KeyboardThruster] Component Removed" << std::endl;
		}

		void KeyboardThruster::awake()
		{
			rigidBody_ = gameObject.lock()->getComponent<RigidBody>();
			force_ = 1000.0f;
			torque_ = 40.0f;
		}

		void KeyboardThruster::update()
		{
			if (!rigidBody_.expired())
			{
				weak<Camera> currentCamera = Camera::getCurrent();

				glm::mat4 viewMatrix = glm::inverse(Math::getTransformation(
					currentCamera.lock()->getTransform().lock()->getPosition(),
					currentCamera.lock()->getTransform().lock()->getRotation(),
					currentCamera.lock()->getTransform().lock()->getScale()
				));

				//Determines normalized vector for directly forward of the camera
				glm::vec3 forward = glm::vec3(viewMatrix[0][2], viewMatrix[1][2], viewMatrix[2][2]);
				forward = glm::normalize(forward);

				if (Input::getKey(TUVEY_KEY_w))
				{
					rigidBody_.lock()->addForce(-forward * (force_ * (float)Time::getDeltaTime()));
				}
				if (Input::getKey(TUVEY_KEY_a))
				{
					rigidBody_.lock()->addTorque(glm::vec3(0.0f, 1.0f, 0.0f) * (torque_* (float)Time::getDeltaTime()));
				}
				if (Input::getKey(TUVEY_KEY_s))
				{
					rigidBody_.lock()->addForce(forward * (force_ * (float)Time::getDeltaTime()));
				}
				if (Input::getKey(TUVEY_KEY_d))
				{
					rigidBody_.lock()->addTorque(glm::vec3(0.0f, -1.0f, 0.0f) * (torque_* (float)Time::getDeltaTime()));
				}
				if (Input::getKey(TUVEY_KEY_e))
				{
					rigidBody_.lock()->addTorque(glm::vec3(1.0f, 0.0f, 0.0f) * (torque_ * (float)Time::getDeltaTime()));
				}
				if (Input::getKey(TUVEY_KEY_q))
				{
					rigidBody_.lock()->addTorque(glm::vec3(-1.0f, 0.0f, 0.0f) * (torque_ * (float)Time::getDeltaTime()));
				}
			}
		}
	}
}