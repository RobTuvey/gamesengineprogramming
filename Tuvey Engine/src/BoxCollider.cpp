#include <gtc/type_ptr.hpp>

#include "Bounds.h"
#include "BoxCollider.h"
#include "Camera.h"
#include "Collision.h"
#include "GameObject.h"
#include "Matht.h"
#include "PlaneCollider.h"
#include "RigidBody.h"
#include "SphereCollider.h"

namespace tuvey
{
	namespace engine
	{
		BoxCollider::BoxCollider()
		{
			std::cout << "++ [BoxCollider] Component Added" << std::endl;
		}

		BoxCollider::~BoxCollider()
		{
			std::cout << "-- [BoxCollider] Component Removed" << std::endl;
		}

		void BoxCollider::awake()
		{
			updateBounds();

			//Determines the vertices of the box collider using the extents of the bounds
			std::vector<glm::vec3> boxVertices;
			glm::vec3 extents = bounds_.getExtent();
			boxVertices.push_back(bounds_.getExtent());
			boxVertices.push_back(glm::vec3(-extents.x, extents.y, extents.z));
			boxVertices.push_back(glm::vec3(extents.x, extents.y, -extents.z));
			boxVertices.push_back(glm::vec3(-extents.x, extents.y, -extents.z));
			boxVertices.push_back(glm::vec3(extents.x, -extents.y, extents.z));
			boxVertices.push_back(glm::vec3(-extents.x, -extents.y, extents.z));
			boxVertices.push_back(glm::vec3(extents.x, -extents.y, -extents.z));
			boxVertices.push_back(-extents);

			vertices_ = boxVertices;

			//pre computes the inverse inertia tensor
			weak<RigidBody> rigidBody = gameObject.lock()->getComponent<RigidBody>();
			if (!rigidBody.expired())
			{
				glm::vec3 size = bounds_.getSize();
				float mass = rigidBody.lock()->getMass();
				float matFloats[9] = {
					(1.0f / 12.0f) * mass * ((size.y * size.y) + (size.z * size.z)), 0.0f, 0.0f,
					0.0f, (1.0f / 12.0f) * mass * ((size.x * size.x) + (size.z * size.z)), 0.0f,
					0.0f, 0.0f, (1.0f / 12.0f) * mass * ((size.x * size.x) + (size.y * size.y))
				};

				invInertiaTensor_ = glm::make_mat3(matFloats);
				invInertiaTensor_ = glm::inverse(invInertiaTensor_);
			}
		}

		void BoxCollider::update()
		{
			std::vector<weak<Collider>> colliders = GameObject::findComponentsOfType<Collider>();

			for (size_t i = 0; i < colliders.size(); i++)
			{
				std::string thisName = gameObject.lock()->getName();
				std::string otherName = colliders.at(i).lock()->gameObject.lock()->getName();
				if (otherName != gameObject.lock()->getName())
				{
					weak<BoxCollider> boxCollider = std::dynamic_pointer_cast<BoxCollider>(colliders.at(i).lock());
					if (!boxCollider.expired())
					{
						if (intersectsBox(boxCollider))
						{
							gameObject.lock()->onCollision(boxCollider);

							std::cout << "-- [Box] Collision Logged" << std::endl;
						}
					}
					else
					{
						weak<SphereCollider> sphereCollider = std::dynamic_pointer_cast<SphereCollider>(colliders.at(i).lock());

						if (!sphereCollider.expired())
						{
							if (intersectsSphere(sphereCollider))
							{
								gameObject.lock()->onCollision(sphereCollider);

								std::cout << "-- [Box] Collision Logged" << std::endl;
							}
						}
						else
						{
							weak<PlaneCollider> planeCollider = std::dynamic_pointer_cast<PlaneCollider>(colliders.at(i).lock());

							if (!planeCollider.expired())
							{
								if (intersectsPlane(planeCollider))
								{
									gameObject.lock()->onCollision(planeCollider);

									std::cout << "-- [Box] Collision Logged" << std::endl;
								}
							}
						}
					}
				}
			}
		}

		glm::vec3 BoxCollider::getCentre()
		{
			return bounds_.getCentre();
		}

		glm::vec3 BoxCollider::getNormal(glm::vec3 point, Bounds otherBounds, glm::vec3 otherPosition)
		{
			//determines the normal of the face of the box collider
			glm::vec3 localPoint = point - (otherBounds.getCentre() + otherPosition);
			float distance = std::abs(otherBounds.getSize().x - std::abs(localPoint.x));
			float min = distance;
			glm::vec3 normal = glm::vec3(1.0f, 0.0f, 0.0f);
			normal = normal * (localPoint.x / localPoint.x);

			distance = std::abs(otherBounds.getSize().y - std::abs(localPoint.y));

			if (distance < min)
			{
				min = distance;
				normal = glm::vec3(0.0f, 1.0f, 0.0f);
				normal = normal * (localPoint.y / localPoint.y);
			}

			distance = std::abs(otherBounds.getSize().z - std::abs(localPoint.z));

			if (distance < min)
			{
				min = distance;
				normal = glm::vec3(0.0f, 0.0f, 1.0f);
				normal = normal * (localPoint.z / localPoint.z);
			}

			return normal;
		}

		Bounds BoxCollider::getBounds()
		{
			return bounds_;
		}

		bool BoxCollider::intersectsBox(weak<BoxCollider> other)
		{
			glm::vec3 thisPosition = gameObject.lock()->getComponent<Transform>()->getPosition();
			glm::vec3 otherPosition = other.lock()->gameObject.lock()->getComponent<Transform>()->getPosition();

			glm::vec3 min = bounds_.getMin() + thisPosition;
			glm::vec3 max = bounds_.getMax() + thisPosition;

			Bounds otherBounds = other.lock()->getBounds();
			glm::vec3 otherMin = otherBounds.getMin() + otherPosition;
			glm::vec3 otherMax = otherBounds.getMax() + otherPosition;

			bool collidesX = false, collidesY = false, collidesZ = false;
			//Determines if the 2 box colliders are intersecting by comparing the minimum and maximum components of the bounds
			if ((min.x >= otherMin.x && min.x <= otherMax.x) || (max.x >= otherMin.x && max.x <= otherMax.x))
			{
				collidesX = true;
			}

			if ((min.y >= otherMin.y && min.y <= otherMax.y) || (max.y >= otherMin.y && max.y <= otherMax.y))
			{
				collidesY = true;
			}

			if ((min.z >= otherMin.z && min.z <= otherMax.z) || (max.z >= otherMin.z && max.z <= otherMax.z))
			{
				collidesZ = true;
			}

			if (collidesX && collidesY && collidesZ)
			{
				latestCollision_ = shared<Collision>(new Collision(shared_from_this(), other, glm::normalize(otherPosition - thisPosition)));
				return true;
			}
			else return false;
		}

		bool BoxCollider::intersectsSphere(weak<SphereCollider> other)
		{
			glm::vec3 thisPosition = gameObject.lock()->getComponent<Transform>()->getPosition();
			glm::vec3 thisCentre = getCentre() + thisPosition;
			glm::vec3 otherCentre = other.lock()->getCentre() + other.lock()->gameObject.lock()->getComponent<Transform>()->getPosition();

			glm::vec3 sphereToBox = otherCentre - thisCentre;
			glm::vec3 normSphereToBox = glm::normalize(sphereToBox);

			glm::vec3 intersectionPoint = normSphereToBox * other.lock()->getRadius() + otherCentre;

			glm::vec3 min = bounds_.getMin() + thisPosition;
			glm::vec3 max = bounds_.getMax() + thisPosition;

			bool collidesX = false, collidesY = false, collidesZ = false;

			//Calculates intersection point of sphere collider and compares it to the minimum
			//and maximum of the box collider.

			if (intersectionPoint.x > min.x && intersectionPoint.x < max.x) collidesX = true;
			if (intersectionPoint.y > min.y && intersectionPoint.y < max.y) collidesY = true;
			if (intersectionPoint.z > min.z && intersectionPoint.z < max.z) collidesZ = true;

			if (Math::magnitude(sphereToBox) < other.lock()->getRadius()) return true;

			if (collidesX && collidesY && collidesZ)
			{
				latestCollision_ = shared<Collision>(new Collision(shared_from_this(), other, normSphereToBox));
				return true;
			}
			else return false;
		}

		bool BoxCollider::intersectsPlane(weak<PlaneCollider> other)
		{
			//Uses the vertices of the box collider and determines what side of the plane it is on.
			//If vertices are present on both sides of the plane then it can be determined an intersection
			//is present
			std::vector<glm::vec3> worldVertices = translateToWorld(vertices_);

			glm::vec3 normal = other.lock()->getNormal();

			bool gotPositive = false, gotNegative = false;

			for (size_t i = 0; i < worldVertices.size(); i++)
			{
				glm::vec3 pointToPlane = worldVertices.at(i) - other.lock()->getTransform().lock()->getPosition();
				float dot = glm::dot(normal, pointToPlane);

				if (dot >= 0) gotPositive = true;
				else if (dot < 0) gotNegative = true;
			}

			if (gotPositive && gotNegative)
			{
				latestCollision_ = shared<Collision>(new Collision(shared_from_this(), other, -normal));
				return true;
			}
			else return false;
		}

		std::vector<glm::vec3> BoxCollider::translateToWorld(std::vector<glm::vec3> vertices)
		{
			//translates box vertices to world space
			std::vector<glm::vec3> newVertices;
			glm::vec3 position = gameObject.lock()->getComponent<Transform>()->getPosition();

			for (size_t i = 0; i < vertices.size(); i++)
			{
				newVertices.push_back(vertices.at(i) + position);
			}

			return newVertices;
		}

		bool BoxCollider::checkCollision(weak<Collider> other)
		{
			weak<BoxCollider> boxCollider = std::dynamic_pointer_cast<BoxCollider>(other.lock());
			if (!boxCollider.expired())
			{
				return intersectsBox(boxCollider);
			}
			else
			{
				weak<SphereCollider> sphereCollider = std::dynamic_pointer_cast<SphereCollider>(other.lock());

				if (!sphereCollider.expired())
				{
					return intersectsSphere(sphereCollider);
				}
				else
				{
					weak<PlaneCollider> planeCollider = std::dynamic_pointer_cast<PlaneCollider>(other.lock());

					if (!planeCollider.expired())
					{
						return intersectsPlane(planeCollider);
					}
				}


			}
		}
	}
}