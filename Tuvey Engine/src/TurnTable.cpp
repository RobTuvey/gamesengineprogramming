#include "GameObject.h"
#include "Input.h"
#include "Time.h"
#include "Transform.h"
#include "TurnTable.h"

namespace tuvey
{
	namespace engine
	{
		TurnTable::TurnTable()
		{
			std::cout << "++ [TurnTable] Component Added" << std::endl;
		}

		TurnTable::~TurnTable()
		{
			std::cout << "-- [TurnTable] Component Removed" << std::endl;
		}

		void TurnTable::awake()
		{
			transform_ = gameObject.lock()->getComponent<Transform>();
		}

		void TurnTable::update()
		{
			//Rotates the object based on key press
			if (Input::getKey(TUVEY_KEY_e))
				transform_.lock()->rotate(glm::vec3(0.0f, 1.0f, 0.0f) * (float)Time::getDeltaTime());
			if (Input::getKey(TUVEY_KEY_q))
				transform_.lock()->rotate(glm::vec3(0.0f, -1.0f, 0.0f) * (float)Time::getDeltaTime());
		}
	}
}