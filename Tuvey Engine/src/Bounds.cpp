#include "Bounds.h"

namespace tuvey
{
	namespace engine
	{
		Bounds::Bounds()
		{

		}

		Bounds::Bounds(glm::vec3 centre, glm::vec3 size)
		{
			centre_ = centre;
			size_ = size;
			extent_ = size / 2.0f;
			min_ = centre_ - extent_;
			max_ = centre_ + extent_;
		}

		Bounds::~Bounds()
		{

		}

		glm::vec3 Bounds::getCentre()
		{
			return centre_;
		}

		glm::vec3 Bounds::getExtent()
		{
			return extent_;
		}

		glm::vec3 Bounds::getSize()
		{
			return size_;
		}

		glm::vec3 Bounds::getMin()
		{
			return min_;
		}

		glm::vec3 Bounds::getMax()
		{
			return max_;
		}

		void Bounds::setMinMax(glm::vec3 min, glm::vec3 max)
		{
			centre_ = (min + max) / 2.0f;
			extent_ = max - centre_;
			size_ = extent_ * 2.0f;
			min_ = min;
			max_ = max;
		}

		void Bounds::setCentre(glm::vec3 centre)
		{
			centre_ = centre;
		}
	}
}