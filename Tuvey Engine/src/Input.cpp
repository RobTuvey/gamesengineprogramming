#include "Input.h"
#include "Application.h"

namespace tuvey
{
	namespace engine
	{
		std::vector<int> Input::keys_;
		std::vector<int> Input::keysDown_;
		std::vector<int> Input::keysUp_;

		int Input::mouseX_;
		int Input::mouseY_;
		bool Input::leftClick_;
		bool Input::rightClick_;

		std::vector<int> Input::buttons_;
		std::vector<int> Input::buttonsDown_;
		std::vector<int> Input::buttonsUp_;
		std::map<int, int> Input::axis_;

		std::map<int, SDL_GameController*> Input::gameControllers_;

		bool Input::getKey(int keyCode)
		{
			for (auto key : keys_)
			{
				if (key == keyCode) return true;
			}

			return false;
		}

		bool Input::getKeyDown(int keyCode)
		{
			for (auto key : keysDown_)
			{
				if (key == keyCode) return true;
			}

			return false;
		}

		bool Input::getKeyUp(int keyCode)
		{
			for (auto key : keysUp_)
			{
				if (key == keyCode) return true;
			}

			return false;
		}

		int Input::getMouseX()
		{
			return mouseX_;
		}

		int Input::getMouseY()
		{
			return mouseY_;
		}

		bool Input::getLeftClick()
		{
			return leftClick_;
		}

		bool Input::getRightClick()
		{
			return rightClick_;
		}

		bool Input::getButton(int buttonId)
		{
			for (auto button : buttons_)
			{
				if (button == buttonId)
					return true;
			}

			return false;
		}

		bool Input::getButtonDown(int buttonId)
		{
			for (auto button : buttonsDown_)
			{
				if (button == buttonId)
					return true;
			}

			return false;
		}

		bool Input::getButtonUp(int buttonId)
		{
			for (auto button : buttonsUp_)
			{
				if (button == buttonId)
					return true;
			}

			return false;
		}

		int Input::getAxis(int axisId)
		{
			return axis_[axisId];
		}

		SDL_GameController* Input::getController(int deviceId)
		{
			return gameControllers_[deviceId];
		}

		void Input::resetUpAndDownKeys()
		{
			keysDown_.clear();
			keysUp_.clear();
			
			buttonsDown_.clear();
			buttonsUp_.clear();
		}

		void Input::getInput()
		{
			int inputNo = 0;
			bool alreadyPress = false;
			SDL_Event sdlEvent;

			SDL_GetMouseState(&mouseX_, &mouseY_);

			while (SDL_PollEvent(&sdlEvent))
			{
				switch (sdlEvent.type)
				{
				case SDL_QUIT:
					Application::context_->quit = true;
					break;
				case SDL_KEYDOWN:
					//If the key is already present in the list it won't be added again
					inputNo = sdlEvent.key.keysym.sym;					

					for (size_t i = 0; i < keys_.size(); i++)
					{
						if (keys_.at(i) == inputNo)
							alreadyPress = true;
					}

					if (!alreadyPress)
					{
						keys_.push_back(inputNo);
						keysDown_.push_back(inputNo);
					}
					break;
				case SDL_KEYUP:
					inputNo = sdlEvent.key.keysym.sym;

					keysUp_.push_back(inputNo);

					for (size_t i = 0; i < keys_.size(); i++)
					{
						if (inputNo == keys_.at(i))
							keys_.erase(keys_.begin() + i);
					}
					break;
				case SDL_MOUSEBUTTONDOWN:
					if (sdlEvent.button.button == SDL_BUTTON_LEFT)
						leftClick_ = true;
					
					if (sdlEvent.button.button == SDL_BUTTON_RIGHT)
						rightClick_ = true;
					break;
				case SDL_MOUSEBUTTONUP:
					if (sdlEvent.button.button == SDL_BUTTON_LEFT)
						leftClick_ = false;

					if (sdlEvent.button.button == SDL_BUTTON_RIGHT)
						rightClick_ = false;
					break;
				case SDL_MOUSEMOTION:
					
					break;
				case SDL_CONTROLLERDEVICEADDED:
					addController(sdlEvent.cdevice.which);
					break;
				case SDL_CONTROLLERDEVICEREMOVED:
					removeController(sdlEvent.cdevice.which);
					break;
				case SDL_CONTROLLERBUTTONDOWN:
					inputNo = sdlEvent.cbutton.button;
					for (size_t i = 0; i < buttons_.size(); i++)
					{
						if (buttons_.at(i) == inputNo)
							alreadyPress = true;
					}

					if (!alreadyPress)
					{
						buttons_.push_back(inputNo);
						buttonsDown_.push_back(inputNo);
					}
					break;
				case SDL_CONTROLLERBUTTONUP:
					inputNo = sdlEvent.cbutton.button;

					buttonsUp_.push_back(inputNo);

					for (size_t i = 0; i < buttons_.size(); i++)
					{
						if (buttons_.at(i) == inputNo)
							buttons_.erase(buttons_.begin() + i);
					}
					break;

				}
			}
		}

		void Input::addController(int id)
		{
			if (SDL_IsGameController(id))
			{
				SDL_GameController* gameController = SDL_GameControllerOpen(id);

				if (gameController)
					gameControllers_[id] = gameController;
			}
		}

		void Input::removeController(int id)
		{
			SDL_GameController* gameController = gameControllers_[id];
			if (gameController)
				SDL_GameControllerClose(gameController);
		}
	}
}