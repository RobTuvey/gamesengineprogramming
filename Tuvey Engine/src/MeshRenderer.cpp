#include <glm.hpp>
#include <gtc/matrix_inverse.hpp>
#include <gtc/matrix_transform.hpp>
#include <iostream>

#include "Camera.h"
#include "GameObject.h"
#include "Material.h"
#include "Matht.h"
#include "MeshComponent.h"
#include "MeshRenderer.h"
#include "Shader.h"
#include "Transform.h"

namespace tuvey
{
	namespace engine
	{
		MeshRenderer::MeshRenderer()
		{
			std::cout << "++ [MeshRenderer] Component Added" << std::endl;

			material_ = shared<Material>(new Material(".\\Shaders\\standardVertex.vert", ".\\Shaders\\standardFragment.frag"));
			ambientColour_ = glm::vec3(1.0f, 0.0f, 0.0f);
		}

		MeshRenderer::MeshRenderer(shared<Material> material)
		{
			std::cout << "++ [MeshRenderer] Component Added" << std::endl;

			material_ = material;
			ambientColour_ = glm::vec3(1.0f, 1.0f, 1.0f);
		}

		MeshRenderer::~MeshRenderer()
		{
			std::cout << "-- [MeshRenderer] Component Removed" << std::endl;
		}

		void MeshRenderer::start()
		{

		}

		void MeshRenderer::awake()
		{
			mesh_ = gameObject.lock()->getComponent<MeshComponent>()->getMesh();
		}

		void MeshRenderer::render()
		{
			//Computes model and view matrix
			weak<Camera> currentCamera = Camera::getCurrent();
			glm::mat4 projectionMatrix = currentCamera.lock()->getProjectionMatrix();

			glm::mat4 viewMatrix = glm::inverse(Math::getTransformation(
				currentCamera.lock()->getTransform().lock()->getPosition(),
				currentCamera.lock()->getTransform().lock()->getRotation(),
				currentCamera.lock()->getTransform().lock()->getScale()
			));

			glm::mat4 modelMatrix = Math::getTransformation(
				getTransform().lock()->getPosition(),
				getTransform().lock()->getRotation(),
				getTransform().lock()->getScale()
			);

			glm::mat4 normalMatrix = glm::transpose(viewMatrix * glm::inverse(modelMatrix));

			glm::vec3 cameraPos = currentCamera.lock()->getTransform().lock()->getPosition();
			//Set variables ready to be inserted into the shader
			material_->setMatrix("in_Projection", projectionMatrix);
			material_->setMatrix("in_View", viewMatrix);
			material_->setMatrix("in_Model", modelMatrix);
			material_->setMatrix("in_NormalMatrix", glm::transpose(viewMatrix * glm::inverse(modelMatrix)));
			material_->setVector3("in_Light", glm::vec3(-4.0f, 5.0f, 10.0f));
			material_->setVector4("in_Colour", glm::vec4(ambientColour_.x, ambientColour_.y, ambientColour_.z, 1));

			material_->insertData();

			GLuint positionAttribID = material_->getPositionId();
			GLuint normalAttribID = material_->getNormalId();
			GLuint uvAttribID = material_->getUvId();

			if (material_->getTextureId() != 0)
			{
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_ARRAY_BUFFER, material_->getTextureId());
			}
			//Enables the attribute arrays and binds buffers containing the data for
			//the vertices, uvs and normals
			glEnableVertexAttribArray(positionAttribID);
			glBindBuffer(GL_ARRAY_BUFFER, mesh_.lock()->getVertexBuffer());
			glVertexAttribPointer(positionAttribID, 3, GL_FLOAT, GL_FALSE, 0, 0);

			if (normalAttribID != -1 && mesh_.lock()->getNormals().size() > 0)
			{
				glEnableVertexAttribArray(normalAttribID);
				glBindBuffer(GL_ARRAY_BUFFER, mesh_.lock()->getNormalBuffer());
				glVertexAttribPointer(normalAttribID, 3, GL_FLOAT, GL_FALSE, 0, 0);
			}

			if (uvAttribID != -1 && mesh_.lock()->getUVs().size() > 0)
			{
				glEnableVertexAttribArray(uvAttribID);
				glBindBuffer(GL_ARRAY_BUFFER, mesh_.lock()->getUvBuffer());
				glVertexAttribPointer(uvAttribID, 2, GL_FLOAT, GL_FALSE, 0, 0);
			}

			glDrawArrays(GL_TRIANGLES, 0, mesh_.lock()->getVertices().size());
			GLenum error = glGetError();

			glDisableVertexAttribArray(positionAttribID);
			glDisableVertexAttribArray(uvAttribID);
			glDisableVertexAttribArray(normalAttribID);
		}

		void MeshRenderer::destroy()
		{
			mesh_.reset();
			material_.reset();
		}

		glm::vec3 MeshRenderer::getAmbientColour()
		{
			return ambientColour_;
		}

		shared<Material> MeshRenderer::getMaterial()
		{
			return material_;
		}

		void MeshRenderer::setAmbientColour(glm::vec3 colour)
		{
			ambientColour_ = colour;
		}

		void MeshRenderer::setMaterial(shared<Material> material)
		{
			material_ = material;
		}
	}
}