#include "Collision.h"
#include "Collider.h"

namespace tuvey
{
	namespace engine
	{
		Collision::Collision()
		{

		}

		Collision::Collision(weak<Collider> A, weak<Collider> B, glm::vec3 normal)
		{
			colliderA_ = A;
			colliderB_ = B;
			normal_ = normal;
		}

		Collision::~Collision()
		{

		}

		glm::vec3 Collision::getNormal()
		{
			return normal_;
		}

		weak<Collider> Collision::getColliderA()
		{
			return colliderA_;
		}

		weak<Collider> Collision::getColliderB()
		{
			return colliderB_;
		}
	}
}