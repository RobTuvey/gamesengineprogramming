#include <iostream>

#include "Application.h"
#include "BoxCollider.h"
#include "Camera.h"
#include "FileManager.h"
#include "GameObject.h"
#include "KeyboardThruster.h"
#include "Material.h"
#include "Mesh.h"
#include "MeshComponent.h"
#include "MeshRenderer.h"
#include "Plane.h"
#include "RigidBody.h"
#include "SphereCollider.h"
#include "Transform.h"
#include "TurnTable.h"

namespace tuvey
{
	namespace engine
	{
		void _main(int argc, char* argv[])
		{

			Application::init();

			GameObject* MainCamera = new GameObject("Main Camera");
			MainCamera->addComponent<Camera>();
			MainCamera->getComponent<Transform>()->setPosition(glm::vec3(0.0f, 1.0f, 10.0f));
			MainCamera->getComponent<Transform>()->setRotation(glm::vec3(0.0f, 0.0f, 0.0f));

			shared<Mesh> boxMesh = shared<Mesh>(FileManager::loadObj(".\\Models\\box.obj"));
			shared<Mesh> sphereMesh = shared<Mesh>(FileManager::loadObj(".\\Models\\sphere.obj"));
			shared<Mesh> trooperMesh = shared<Mesh>(FileManager::loadObj(".\\Models\\sphere.obj"));

			GameObject* trooper = new GameObject("Trooper");
			shared<Material> material = shared<Material>(new Material(".\\Shaders\\standardVertex.vert", ".\\Shaders\\simpleTextureFragment.frag"));
			//material->loadTexture(".\\Textures\\trooperTexture.png");

			material->setFloat("in_SpecInt", 0.5f);
			material->setFloat("in_DiffInt", 0.5f);

			trooper->getComponent<Transform>()->setPosition(glm::vec3(0.0f, -1.0f, 5.0f));
			trooper->addComponent<MeshComponent>()->setMesh(trooperMesh);
			trooper->addComponent<MeshRenderer>()->setMaterial(material);
			trooper->addComponent<TurnTable>();

			GameObject* plane = Plane::create();

			Application::run();

			Application::destroy();

			return;
		}
	}
}

int main(int argc, char* argv[])
{
	tuvey::engine::_main(argc, argv);

	return 0;
}