#include "BoxCollider.h"
#include "Collider.h"
#include "Collision.h"
#include "GameObject.h"
#include "Matht.h"
#include "Mesh.h"
#include "MeshComponent.h"
#include "RigidBody.h"
#include "SphereCollider.h"
#include "Time.h"
#include "Transform.h"

namespace tuvey
{
	namespace engine
	{
		RigidBody::RigidBody()
		{
			std::cout << "++ [RigidBody] Component Added" << std::endl;

			usesGravity_ = false;
			isKinematic_ = true;
			gravity_ = 1;
			gravityComponent_ = gravity_ * 9.8f;
			drag_ = 1.5;
			angularDrag_ = 1;
			mass_ = 1;
			elasticity_ = 0.2;
			linearImpulseDampener_ = 2.0f;
			angularImpulseDampener_ = 10.0f;

			scale_ = 100.0f;
		}

		RigidBody::~RigidBody() 
		{
			std::cout << "-- [RigidBody] Component Removed" << std::endl;
		}

		void RigidBody::awake()
		{
			transform_ = gameObject.lock()->getComponent<Transform>();
			if (gameObject.lock()->getComponent<Collider>())
				bounds_ = gameObject.lock()->getComponent<MeshComponent>()->getMesh()->getBounds();
		}

		void RigidBody::update()
		{
			if (usesGravity_)
				addForce(glm::vec3(0.0f, gravityComponent_ * mass_, 0.0f));

			addForce((-linearVelocity_ * mass_) * drag_);
			//Update linear velocity with updated force vector
			linearAcceleration_ = force_ / mass_;
			force_ = glm::vec3(0.0f);
			linearVelocity_ += linearAcceleration_ * (float)Time::getDeltaTime();

			if (!transform_.expired()) transform_.lock()->translate(linearVelocity_ / scale_);

			addTorque(-angularVelocity_ * angularDrag_);
			//Update angular velocity with updated torque vector
			angularAcceleration_ = torque_ / (mass_ * (bounds_.getExtent() * bounds_.getExtent()));
			torque_ = glm::vec3(0.0f);
			angularVelocity_ += angularAcceleration_ * (float)Time::getDeltaTime();

			if (!transform_.expired()) transform_.lock()->rotate(angularVelocity_ / scale_);
		}

		void RigidBody::onCollision(weak<Collider> other)
		{
			if (isKinematic_)
			{
				glm::vec3 relativeVelocity;
				weak<RigidBody> o_RigidBody = other.lock()->gameObject.lock()->getComponent<RigidBody>();
				if (!o_RigidBody.expired())
				{
					relativeVelocity = linearVelocity_ - o_RigidBody.lock()->linearVelocity_;
				}
				else relativeVelocity = linearVelocity_;
				
				glm::vec3 collisionNormal = gameObject.lock()->getComponent<Collider>()->getCollisionNormal();

				float j = computeImpulseMagnitude(relativeVelocity, collisionNormal, o_RigidBody);
				
				//update linear velocity with impulse force
				linearVelocity_ += ((j * collisionNormal) / mass_) / linearImpulseDampener_;
				if (!o_RigidBody.expired())
					o_RigidBody.lock()->linearVelocity_ += ((-j * collisionNormal) / o_RigidBody.lock()->mass_) / linearImpulseDampener_;

				//udpate angular velocity with impulse force
				angularVelocity_ += glm::cross(getCollisionDistance(collisionNormal), j * collisionNormal) * getInertiaTensor() / angularImpulseDampener_;
				if (!o_RigidBody.expired())
					o_RigidBody.lock()->angularVelocity_ += glm::cross(o_RigidBody.lock()->getCollisionDistance(collisionNormal), -j * collisionNormal) * o_RigidBody.lock()->getInertiaTensor() / angularImpulseDampener_;

				adjustCollisionPosition(other);
			}
		}

		void RigidBody::adjustCollisionPosition(weak<Collider> other)
		{
			weak<Collider> thisCollider = gameObject.lock()->getComponent<Collider>();
			weak<BoxCollider> boxCollider = std::dynamic_pointer_cast<BoxCollider>(thisCollider.lock());
			glm::vec3 normal = gameObject.lock()->getComponent<Collider>()->getCollisionNormal();
			float step = 0.01f;
			
			//Adjust the postion of the rigid body by a small step, along the collision normal,
			//until it is no longer colliding with the other body.
			
			if (!boxCollider.expired())
			{
				while (boxCollider.lock()->checkCollision(other))
				{
					gameObject.lock()->getComponent<Transform>()->translate(-normal * step);
				}
			}
			else
			{
				weak<SphereCollider> sphereCollider = std::dynamic_pointer_cast<SphereCollider>(thisCollider.lock());

				if (!sphereCollider.expired())
				{
					while (sphereCollider.lock()->checkCollision(other))
					{
						gameObject.lock()->getComponent<Transform>()->translate(-normal * step);
					}
				}
			}
		}

		float RigidBody::getMass()
		{
			return mass_;
		}

		void RigidBody::setGravity(float gravity)
		{
			gravity_ = gravity;
			gravityComponent_ = gravity_ * 9.8f;
		}

		void RigidBody::setDrag(float drag)
		{
			drag_ = drag;
		}

		void RigidBody::setAngularDrag(float angularDrag)
		{
			angularDrag_ = angularDrag;
		}

		void RigidBody::setMass(float mass)
		{
			mass_ = mass;
		}

		void RigidBody::setUsesGravity(bool usesGravity)
		{
			usesGravity_ = usesGravity;
		}

		void RigidBody::addForce(glm::vec3 force)
		{
			if (Math::magnitude(force) > 0) force_ += force;
		}

		void RigidBody::addTorque(glm::vec3 torque)
		{
			if (Math::magnitude(torque) > 0) torque_ += torque;
		}

		float RigidBody::computeImpulseMagnitude(glm::vec3 relativeVelocity, glm::vec3 collisionNormal, weak<RigidBody> other)
		{
			float j = 0;

			float mass1 = mass_;
			float mass2;
			if (!other.expired())
				mass2 = other.lock()->mass_;
			else
				mass2 = 0;

			float jLinearNum = -(elasticity_) * glm::dot(relativeVelocity, collisionNormal);
			float jLinearDen = ((1 / mass1) + (1 / mass2));

			float jLinear = jLinearNum / jLinearDen;

			//Cross product of r1 with the contact normal of body 1. This is then multiplied
			//by 3x3 inverse inertia tensor matrix which results in a new vector3. The cross
			//product of this new vector and r1 give you a final vector for the body. And the 
			//dot product of this vecotr and the contact normal results in the magnitude of
			//the angular impuls for body 1.
			glm::vec3 r1 = getCollisionDistance(collisionNormal);
			float jAngularPart1 = glm::dot(collisionNormal,
				glm::cross(glm::cross(r1,collisionNormal) * getInertiaTensor(), r1));

			//Same again for body 2
			float jAngularPart2 = 0;
			glm::vec3 r2 = glm::vec3(0.0f);
			if (!other.expired())
			{
				r2 = other.lock()->getCollisionDistance(collisionNormal);
				jAngularPart2 = glm::dot(collisionNormal,
					glm::cross(glm::cross(r2, collisionNormal) * other.lock()->getInertiaTensor(), r2));
			}

			float jAngular = -(elasticity_) * glm::dot(relativeVelocity, collisionNormal) * (jAngularPart1 + jAngularPart2);

			j = jLinear + jAngular;

			return j;
		}

		glm::mat3 RigidBody::getInertiaTensor()
		{
			glm::mat3 invInertiaTensor = gameObject.lock()->getComponent<Collider>()->getInvInertiaTensor();

			glm::mat3 rotationMatrix = Math::getRotationMatrix(gameObject.lock()->getComponent<Transform>()->getRotation());
			glm::mat3 tranRotationMatrix = glm::transpose(rotationMatrix);

			return rotationMatrix * invInertiaTensor * tranRotationMatrix;
		}

		glm::vec3 RigidBody::getCollisionDistance(glm::vec3 collisionNormal)
		{
			return bounds_.getExtent();
		}
	}
}