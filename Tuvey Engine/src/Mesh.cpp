#include "Mesh.h"

namespace tuvey
{
	namespace engine
	{
		Mesh::Mesh()
		{

		}

		Mesh::Mesh(std::vector<glm::vec3> vertices, std::vector<glm::vec2> uvs, std::vector<glm::vec3> normals)
		{
			vertices_ = vertices;
			uvs_ = uvs;
			normals_ = normals;

			initializeBuffers();

			recalculateBounds();
		}

		Mesh::~Mesh()
		{

		}

		void Mesh::initializeBuffers()
		{
			//Initializes buffers for the mesh data

			if (vertices_.size() > 0)
			{
				auto banana = &vertices_[0];
				glGenBuffers(1, &vertexBuffer_);
				glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer_);
				glBufferData(GL_ARRAY_BUFFER, vertices_.size() * sizeof(glm::vec3), &vertices_[0], GL_DYNAMIC_DRAW);
			}

			if (uvs_.size() > 0)
			{
				glGenBuffers(1, &uvBuffer_);
				glBindBuffer(GL_ARRAY_BUFFER, uvBuffer_);
				glBufferData(GL_ARRAY_BUFFER, uvs_.size() * sizeof(glm::vec2), &uvs_[0], GL_DYNAMIC_DRAW);
			}

			if (normals_.size() > 0)
			{
				glGenBuffers(1, &normalBuffer_);
				glBindBuffer(GL_ARRAY_BUFFER, normalBuffer_);
				glBufferData(GL_ARRAY_BUFFER, normals_.size() * sizeof(glm::vec3), &normals_[0], GL_DYNAMIC_DRAW);
			}
		}

		void Mesh::setVertices(std::vector<glm::vec3> vertices)
		{
			vertices_ = vertices;
		}

		void Mesh::setUVs(std::vector<glm::vec2> uvs)
		{
			uvs_ = uvs;
		}

		void Mesh::setNormals(std::vector<glm::vec3> normals)
		{
			normals_ = normals;
		}

		std::vector<glm::vec3>& Mesh::getVertices()
		{
			return vertices_;
		}

		std::vector<glm::vec2>& Mesh::getUVs()
		{
			return uvs_;
		}

		std::vector<glm::vec3>& Mesh::getNormals()
		{
			return normals_;
		}

		GLuint Mesh::getVertexBuffer()
		{
			return vertexBuffer_;
		}

		GLuint Mesh::getUvBuffer()
		{
			return uvBuffer_;
		}

		GLuint Mesh::getNormalBuffer()
		{
			return normalBuffer_;
		}

		void Mesh::recalculateBounds()
		{
			//Calculates the bounds of the mesh by finding the smallest and largest possible value on each axis
			if (vertices_.size() < 1)
			{
				bounds_ = Bounds(glm::vec3(0.0f), glm::vec3(0.0f));
			}

			glm::vec3 min = vertices_.at(0);
			glm::vec3 max = vertices_.at(0);

			for (size_t i = 0; i < vertices_.size(); i++)
			{
				if (vertices_.at(i).x < min.x)
				{
					min.x = vertices_.at(i).x;
				}
				else if (vertices_.at(i).x > max.x)
				{
					max.x = vertices_.at(i).x;
				}

				if (vertices_.at(i).y < min.y)
				{
					min.y = vertices_.at(i).y;
				}
				else if (vertices_.at(i).y > max.y)
				{
					max.y = vertices_.at(i).y;
				}

				if (vertices_.at(i).z < min.z)
				{
					min.z = vertices_.at(i).z;
				}
				else if (vertices_.at(i).z > max.z)
				{
					max.z = vertices_.at(i).z;
				}
			}

			bounds_ = Bounds((min + max) / 2.0f, (max - min));
		}

		Bounds Mesh::getBounds()
		{
			return bounds_;
		}
	}
}