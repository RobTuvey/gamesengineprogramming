#include <gtc/type_ptr.hpp>

#include "BoxCollider.h"
#include "Bounds.h"
#include "Collision.h"
#include "GameObject.h"
#include "Matht.h"
#include "PlaneCollider.h"
#include "RigidBody.h"
#include "SphereCollider.h"

namespace tuvey
{
	namespace engine
	{
		SphereCollider::SphereCollider()
		{
			std::cout << "++ [SphereCollider] Component Added" << std::endl;
		}

		SphereCollider::~SphereCollider()
		{
			std::cout << "-- [SphereCollider] Component Removed" << std::endl;
		}

		void SphereCollider::awake()
		{
			updateBounds();

			radius_ = (bounds_.getMax().x - bounds_.getMin().x) / 2.0f;

			//Pre computes inverse inertia tensor
			weak<RigidBody> rigidBody = gameObject.lock()->getComponent<RigidBody>();
			if (!rigidBody.expired())
			{
				float matFloats[9] = {
					(2.0f / 5.0f) * rigidBody.lock()->getMass() * (radius_ * radius_), 0.0f, 0.0f,
					0.0f, (2.0f / 5.0f) * rigidBody.lock()->getMass() * (radius_ * radius_), 0.0f,
					0.0f, 0.0f, (2.0f / 5.0f) * rigidBody.lock()->getMass() * (radius_ * radius_)
				};

				invInertiaTensor_ = glm::make_mat3(matFloats);
				invInertiaTensor_ = glm::inverse(invInertiaTensor_);
			}
		}

		void SphereCollider::update()
		{
			std::vector<weak<Collider>> colliders = GameObject::findComponentsOfType<Collider>();

			bool collides = false;

			for (size_t i = 0; i < colliders.size(); i++)
			{
				if (colliders.at(i).lock()->gameObject.lock()->getName() != gameObject.lock()->getName())
				{
					weak<BoxCollider> boxCollider = std::dynamic_pointer_cast<BoxCollider>(colliders.at(i).lock());

					if (!boxCollider.expired())
					{
						if (intersectsBox(boxCollider))
						{
							gameObject.lock()->onCollision(colliders.at(i));

							std::cout << "-- [SPHERE] Collision logged." << std::endl;
						}
					}
					else
					{
						weak<SphereCollider> sphereCollider = std::dynamic_pointer_cast<SphereCollider>(colliders.at(i).lock());

						if (!sphereCollider.expired())
						{
							if (intersectsSphere(sphereCollider))
							{
								gameObject.lock()->onCollision(colliders.at(i));

								std::cout << "-- [SPHERE] Collision logged." << std::endl;
							}
						}
						else
						{
							weak<PlaneCollider> planeCollider = std::dynamic_pointer_cast<PlaneCollider>(colliders.at(i).lock());

							if (!planeCollider.expired())
							{
								if (intersectsPlane(planeCollider))
								{
									gameObject.lock()->onCollision(colliders.at(i));

									std::cout << "-- [SPHERE] Collision logged." << std::endl;
								}
							}
						}
					}
				}
			}
		}

		glm::vec3 SphereCollider::getCentre()
		{
			return bounds_.getCentre();
		}

		float SphereCollider::getRadius()
		{
			return radius_;
		}

		Bounds SphereCollider::getBounds()
		{
			return bounds_;
		}

		bool SphereCollider::intersectsPlane(weak<PlaneCollider> other)
		{
			//Uses the plane normal to determine the distance of the sphere to the plane and comparing it to the radius of the collider
			glm::vec3 sphereToPlane = (getCentre() + gameObject.lock()->getComponent<Transform>()->getPosition()) - (other.lock()->getTransform().lock()->getPosition());
			float dot = glm::dot(other.lock()->getNormal(), sphereToPlane);

			if (dot < getRadius() && dot > -getRadius())
			{
				latestCollision_ = shared<Collision>(new Collision(shared_from_this(), other, other.lock()->getNormal()));
				return true;
			}
			else return false;
		}

		bool SphereCollider::intersectsBox(weak<BoxCollider> other)
		{
			glm::vec3 thisPosition = gameObject.lock()->getComponent<Transform>()->getPosition();
			glm::vec3 otherPosition = other.lock()->gameObject.lock()->getComponent<Transform>()->getPosition();

			glm::vec3 sphereToBox = otherPosition - thisPosition;
			glm::vec3 normSphereToBox = glm::normalize(sphereToBox);

			glm::vec3 intersectionPoint = normSphereToBox * radius_ + thisPosition;

			Bounds otherBounds = other.lock()->getBounds();
			glm::vec3 min = otherBounds.getMin() + otherPosition;
			glm::vec3 max = otherBounds.getMax() + otherPosition;

			bool collidesX = false, collidesY = false, collidesZ = false;

			//Calculates intersection point of sphere collider and compares it to the minimum
			//and maximum of the box collider.

			if (intersectionPoint.x > min.x && intersectionPoint.x < max.x) collidesX = true;
			if (intersectionPoint.y > min.y && intersectionPoint.y < max.y) collidesY = true;
			if (intersectionPoint.z > min.z && intersectionPoint.z < max.z) collidesZ = true;

			if (Math::magnitude(sphereToBox) < radius_) return true;

			if (collidesX && collidesY && collidesZ)
			{
				latestCollision_ = shared<Collision>(new Collision(shared_from_this(), other, normSphereToBox));

				return true;
			}

			return false;
		}

		bool SphereCollider::intersectsSphere(weak<SphereCollider> other)
		{
			//Compares the sum of the radius of the two colliders to the distance between them to determine if they are intersecting
			glm::vec3 thisPosition = gameObject.lock()->getComponent<Transform>()->getPosition();
			glm::vec3 otherPosition = other.lock()->gameObject.lock()->getComponent<Transform>()->getPosition();

			glm::vec3 sphereToSphere = otherPosition - thisPosition;
			float radiusSum = radius_ + other.lock()->getRadius();

			if (radiusSum > Math::magnitude(sphereToSphere))
			{
				latestCollision_ = shared<Collision>(new Collision(shared_from_this(), other, glm::normalize(sphereToSphere)));
				return true;
			}

			return false;
		}

		bool SphereCollider::checkCollision(weak<Collider> other)
		{
			weak<BoxCollider> boxCollider = std::dynamic_pointer_cast<BoxCollider>(other.lock());

			if (!boxCollider.expired())
			{
				return intersectsBox(boxCollider);
			}
			else
			{
				weak<SphereCollider> sphereCollider = std::dynamic_pointer_cast<SphereCollider>(other.lock());

				if (!sphereCollider.expired())
				{
					return intersectsSphere(sphereCollider);
				}
				else
				{
					weak<PlaneCollider> planeCollider = std::dynamic_pointer_cast<PlaneCollider>(other.lock());

					if (!planeCollider.expired())
					{
						return intersectsPlane(planeCollider);
					}
				}
			}
		}
	}
}