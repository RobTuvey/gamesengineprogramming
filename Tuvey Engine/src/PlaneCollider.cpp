#include <gtx/rotate_vector.hpp>

#include "GameObject.h"
#include "Mesh.h"
#include "MeshComponent.h"
#include "PlaneCollider.h"

namespace tuvey
{
	namespace engine
	{
		PlaneCollider::PlaneCollider()
		{
			std::cout << "++ [PlaneCollider] Component Added" << std::endl;
		}

		PlaneCollider::~PlaneCollider()
		{
			std::cout << "-- [PlaneCollider] Component Removed" << std::endl;
		}

		void PlaneCollider::awake()
		{
			weak<Mesh> mesh = gameObject.lock()->getComponent<MeshComponent>()->getMesh();
			glm::vec3 meshNormal = mesh.lock()->getNormals().at(0);
			glm::vec3 rotation = gameObject.lock()->getComponent<Transform>()->getRotation();

			//Updates the plane normal to consider the rotation of the object
			meshNormal = glm::rotate(meshNormal, rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
			meshNormal = glm::rotate(meshNormal, rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
			meshNormal = glm::rotate(meshNormal, rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));

			normal_ = meshNormal;
		}

		glm::vec3 PlaneCollider::getNormal()
		{
			return normal_;
		}
	}
}