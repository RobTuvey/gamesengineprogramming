#include <iostream>
#include <glm.hpp>

#include "FileManager.h"
#include "GameObject.h"
#include "Material.h"
#include "Mesh.h"
#include "MeshRenderer.h"
#include "MeshComponent.h"
#include "Plane.h"
#include "PlaneCollider.h"
#include "Transform.h"

namespace tuvey
{
	namespace engine
	{
		int Plane::planeNo = 0;

		GameObject* Plane::create()
		{
			GameObject* plane = new GameObject("Plane" + std::to_string(planeNo + 1));
			planeNo++;

			weak<Transform> transform = plane->getComponent<Transform>();
			transform.lock()->setPosition(glm::vec3(0.0f, -1.0f, 0.0f));
			transform.lock()->setLocalScale(glm::vec3(10.0f, 10.0f, 1.0f));

			shared<Mesh> planeMesh = shared<Mesh>(FileManager::loadObj(".\\Models\\plane.obj"));
			plane->addComponent<MeshComponent>()->setMesh(planeMesh);

			plane->addComponent<MeshRenderer>()->setAmbientColour(glm::vec3(150.0f / 255.0f, 150.0f / 255.0f, 150.0f / 255.0f));
			plane->getComponent<MeshRenderer>()->getMaterial()->setFloat("in_SpecInt", 0.5f);
			plane->getComponent<MeshRenderer>()->getMaterial()->setFloat("in_DiffInt", 5.0f);

			plane->addComponent<PlaneCollider>();

			return plane;
		}
	}
}