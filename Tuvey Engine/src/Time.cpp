#include "Time.h"

namespace tuvey
{
	namespace engine
	{
		duration<double> Time::deltaTime_;
		time_point<high_resolution_clock> Time::lastTime_;
		double Time::sinceLastDisplay_;

		void Time::updateDeltaTime()
		{
			//Updates the delta time by comparing the current time to the timne recorded at the last check
			time_point<high_resolution_clock> now = high_resolution_clock::now();

			deltaTime_ = duration_cast<duration<double>>(now - lastTime_);

			lastTime_ = now;
			sinceLastDisplay_ += deltaTime_.count();
		}

		double Time::getDeltaTime()
		{
			return deltaTime_.count();
		}

		double Time::getSinceLastDisplay()
		{
			return sinceLastDisplay_;
		}

		void Time::resetSinceLastDisplay()
		{
			sinceLastDisplay_ = 0.0f;
		}
	}
}