#include "Collider.h"
#include "Component.h"
#include "GameObject.h"
#include "Transform.h"

namespace tuvey
{
	namespace engine
	{
		void Component::start()
		{
			
		}

		void Component::awake()
		{

		}

		void Component::update()
		{

		}

		void Component::render()
		{

		}

		void Component::destroy()
		{

		}

		void Component::onCollision(weak<Collider> other)
		{

		}

		weak<Transform> Component::getTransform()
		{
			return gameObject.lock()->getComponent<Transform>();
		}
	}
}