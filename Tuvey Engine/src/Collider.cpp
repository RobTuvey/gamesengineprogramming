#include <glm.hpp>

#include "Collider.h"
#include "Collision.h"
#include "GameObject.h"
#include "Mesh.h"
#include "MeshComponent.h"

namespace tuvey
{
	namespace engine
	{
		Collider::Collider()
		{

		}

		Collider::~Collider()
		{

		}

		void Collider::awake()
		{
			updateBounds();
		}

		void Collider::update()
		{

		}

		void Collider::updateBounds()
		{
			weak<MeshComponent> meshComp = gameObject.lock()->getComponent<MeshComponent>();

			if (!meshComp.expired())
			{
				weak<Mesh> mesh = meshComp.lock()->getMesh();

				if (!mesh.expired())
				{
					bounds_ = mesh.lock()->getBounds();

					//Updates the bounds and considers the object's scale to get accurate min and max

					glm::vec3 min = bounds_.getMin();
					min *= gameObject.lock()->getComponent<Transform>()->getScale();
					
					glm::vec3 max = bounds_.getMax();
					max *= gameObject.lock()->getComponent<Transform>()->getScale();

					bounds_.setMinMax(min, max);
				}
			}
		}

		Bounds Collider::getBounds()
		{
			return bounds_;
		}

		glm::vec3 Collider::getCollisionNormal()
		{
			return latestCollision_->getNormal();
		}

		glm::mat3 Collider::getInvInertiaTensor()
		{
			return invInertiaTensor_;
		}

		bool Collider::checkCollision(weak<Collider> other)
		{
			return false;
		}
	}
}