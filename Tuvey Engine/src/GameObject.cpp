#include <iostream>

#include "Application.h"
#include "Collider.h"
#include "GameObject.h"
#include "Transform.h"

namespace tuvey
{
	namespace engine
	{
		GameObject::GameObject()
		{
			int objectNo = Application::context_->gameObjects.size() + 1;
			name_ = "GameObject" + std::to_string(objectNo);
			std::cout << "GameObject created: " << name_ << std::endl;

			Application::context_->gameObjects.push_back(shared<GameObject>(this));

			addComponent<Transform>();
		}

		GameObject::GameObject(std::string name)
		{
			std::cout << "GameObject created: " << name << std::endl;
			name_ = name;

			Application::context_->gameObjects.push_back(shared<GameObject>(this));
			
			addComponent<Transform>();
		}

		GameObject::~GameObject()
		{
			std::cout << "GameObject destroyed: " << name_ << std::endl;
		}

		std::string GameObject::getName()
		{
			return name_;
		}

		void GameObject::start()
		{
			for (auto component : components_)
			{
				component->start();
			}
		}

		void GameObject::awake()
		{
			for (auto component : components_)
			{
				component->awake();
			}
		}

		void GameObject::update()
		{
			for (auto component : components_)
			{
				component->update();
			}
		}

		void GameObject::render()
		{
			for (auto component : components_)
			{
				component->render();
			}
		}

		void GameObject::destroy()
		{
			for (size_t i = 0; i < components_.size(); i++)
			{
				components_.at(i)->destroy();
				components_.erase(components_.begin() + i);
				i--;
			}
		}

		void GameObject::onCollision(weak<Collider> other)
		{
			for (auto component : components_)
			{
				component->onCollision(other);
			}
		}
	}
}