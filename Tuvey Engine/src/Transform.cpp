#include <gtc/matrix_inverse.hpp>

#include "Matht.h"
#include "Transform.h"

namespace tuvey
{
	namespace engine
	{
		Transform::Transform()
		{
			localPosition_ = glm::vec3(0.0f);
			localScale_ = glm::vec3(1.0f);
			localRotation_ = glm::vec3(0.0f);

			std::cout << "++ [Transform] Component Added" << std::endl;
		}

		Transform::~Transform()
		{
			std::cout << "-- [Transform] Component Removed" << std::endl;
		}

		void Transform::setParent(weak<Transform> parent)
		{
			parent_ = parent;
		}

		glm::vec3 Transform::getLocalPosition()
		{
			return localPosition_;
		}

		glm::vec3 Transform::getLocalRotation()
		{
			return localRotation_;
		}

		glm::vec3 Transform::getLocalScale()
		{
			return localScale_;
		}

		glm::vec3 Transform::getPosition()
		{
			//Gets the transform positon in world space by applying the transformations of the parent objects
			glm::mat4 transformation = glm::mat4();

			weak<Transform> transform = shared_from_this();
			while (!transform.expired())
			{
				transformation = Math::getTransformation(transform.lock()->getLocalPosition(), transform.lock()->getLocalRotation(), transform.lock()->getLocalScale()) * transformation;
				transform = transform.lock()->parent_;
			}

			return glm::vec3(transformation[3].x, transformation[3].y, transformation[3].z);
		}

		glm::vec3 Transform::getRotation()
		{
			//Gets the rotation of the transform in world space by adding the rotations of the parent objects
			glm::vec3 rotation = localRotation_;

			weak<Transform> parent = parent_;
			while (!parent.expired())
			{
				rotation += parent.lock()->localRotation_;
				parent = parent.lock()->parent_;
			}

			return rotation;
		}

		glm::vec3 Transform::getScale()
		{
			//Gets the scale of the transform in world space by considering the scale of the parent objects
			if (parent_.lock())
			{
				glm::vec3 scale = localScale_;
				
				weak<Transform> parent = parent_;
				while (!parent.expired())
				{
					scale *= parent.lock()->localScale_;
					parent = parent.lock()->parent_;
				}

				return scale;
			}
			else return localScale_;
		}

		void Transform::setLocalPosition(glm::vec3 vector)
		{
			localPosition_ = vector;
		}

		void Transform::setLocalRotation(glm::vec3 vector)
		{
			localRotation_ = vector;
		}

		void Transform::setLocalScale(glm::vec3 vector)
		{
			localScale_ = vector;
		}

		void Transform::setPosition(glm::vec3 vector)
		{
			//Sets the world position of the transform by traversing the parents and transforming the positon
			if (!parent_.expired())
			{
				glm::mat4 transformation = glm::mat4();

				weak<Transform> transform = parent_;
				while (!transform.expired())
				{
					transformation = Math::getTransformation(transform.lock()->getPosition(), transform.lock()->getRotation(), transform.lock()->getScale()) * transformation;
					transform = transform.lock()->parent_;
				}

				glm::vec4 position(vector.x, vector.y, vector.z, 1.0f);
				position = glm::inverseTranspose(transformation) * position;

				localPosition_ = glm::vec3(position.x, position.y, position.z);
			}
			else localPosition_ = vector;
		}

		void Transform::setRotation(glm::vec3 vector)
		{
			if (!parent_.expired())
			{
				localRotation_ = vector - parent_.lock()->getRotation();
			}
			else localRotation_ = vector;
		}

		void Transform::translate(glm::vec3 vector)
		{
			setPosition(getPosition() + vector);
		}

		void Transform::rotate(glm::vec3 vector)
		{
			setRotation(getRotation() + vector);
		}
	}
}