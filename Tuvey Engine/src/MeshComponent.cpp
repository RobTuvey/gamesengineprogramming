#include "MeshComponent.h"

namespace tuvey
{
	namespace engine
	{
		MeshComponent::MeshComponent()
		{
			std::cout << "++ [MeshComponent] Component Added" << std::endl;
		}

		MeshComponent::~MeshComponent()
		{
			std::cout << "-- [MeshComponent] Component Removed" << std::endl;
		}

		void MeshComponent::destroy()
		{
			mesh_.reset();
		}

		shared<Mesh> MeshComponent::getMesh()
		{
			return mesh_;
		}

		void MeshComponent::setMesh(shared<Mesh> mesh)
		{
			mesh_ = mesh;
		}
	}
}