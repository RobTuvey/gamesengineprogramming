#include <iostream>

#include "Application.h"
#include "GameObject.h"
#include "Input.h"
#include "Screen.h"
#include "SDL_image.h"
#include "Time.h"

namespace tuvey
{
	namespace engine
	{
		shared<Context> Application::context_;
		float Application::idealFrameTime_ = 1.0f / 60.0f;

		void Application::init()
		{
			Screen::width = 1280;
			Screen::height = 720;
			//Initializes the application context
			context_.reset(new Context());

			context_->quit = false;
			context_->started = false;

			bool sdlInitialized = initSdl();
			if (!sdlInitialized)
				return;

			bool openGlInitialized = initOpenGl();
			if (!openGlInitialized)
				return;
		}

		void Application::run()
		{
			while (!context_->quit)
			{
				update();

				if (Time::getSinceLastDisplay() > idealFrameTime_)
					render();
			}
		}

		void Application::update()
		{
			Time::updateDeltaTime();

			Input::getInput();

			if (context_->started)
			{
				for (auto gameObject : context_->gameObjects)
				{
					gameObject->update();
				}
			}
			else
			{
				context_->started = true;

				for (auto gameObject : context_->gameObjects)
				{
					gameObject->start();
				}

				for (auto gameObject : context_->gameObjects)
				{
					gameObject->awake();
				}
			}
		}

		void Application::render()
		{
			glClearColor(20.0f / 255.0f, 20.0f / 255.0f, 20.0f / 255.0f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			glViewport(0, 0, Screen::getWidth(), Screen::getHeight());

			if (context_->started)
			{
				for (auto gameObject : context_->gameObjects)
				{
					gameObject->render();
				}
			}

			SDL_GL_SwapWindow(context_->mainWindow);

			Input::resetUpAndDownKeys();
		}

		void Application::destroy()
		{
			for (size_t i = 0; i < context_->gameObjects.size(); i++)
			{
				context_->gameObjects.at(i)->destroy();
				context_->gameObjects.erase(context_->gameObjects.begin() + i);
				i--;
			}

			SDL_GL_DeleteContext(context_->glContext);
			SDL_DestroyRenderer(context_->mainRenderer);
			SDL_DestroyWindow(context_->mainWindow);

			IMG_Quit();

			SDL_Quit();
		}

		bool Application::initOpenGl()
		{
			//Initializes OpenGL
			glewExperimental = true;

			GLenum openGlErr = glewInit();
			if (GLEW_OK != openGlErr)
			{
				std::cout << "GLEW failed to initialise, GLEW Error: " << glewGetErrorString(openGlErr) << std::endl;
				return false;
			}

			std::cout << "INFO: Using GLEW: " << glewGetString(GLEW_VERSION) << std::endl;
			std::cout << "INFO: OpenGL Vendor: " << glGetString(GL_VENDOR) << std::endl;
			std::cout << "INFO: OpenGL Renderer: " << glGetString(GL_RENDERER) << std::endl;
			std::cout << "INFO: OpenGL Version: " << glGetString(GL_VERSION) << std::endl;
			std::cout << "INFO: OpenGL Shading Language Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;

			glEnable(GL_DEPTH_TEST);
			glEnable(GL_CULL_FACE);

			return true;
		}

		bool Application::initSdl()
		{
			//Initializes SDL and sets the OpenGL context's version numbers
			if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
			{
				std::cout << "SDL could not be initialzied, SDL Error: " << SDL_GetError() << std::endl;
				return false;
			}

			if (IMG_Init(IMG_INIT_JPG | IMG_INIT_PNG) < 0)
			{
				std::cout << "SDL_Image could not be initialized, SDL_Image Error: " << IMG_GetError() << std::endl;
			}

			context_->mainWindow = SDL_CreateWindow("Tuvey Engine", 100, 100, Screen::width, Screen::height, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL);
			context_->mainRenderer = SDL_CreateRenderer(context_->mainWindow, -1, 0);
			context_->glContext = SDL_GL_CreateContext(context_->mainWindow);

			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
			SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

			return true;
		}
	}
}