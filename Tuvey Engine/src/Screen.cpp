#include "Screen.h"

namespace tuvey
{
	namespace engine
	{
		int Screen::width;
		int Screen::height;

		int Screen::getWidth()
		{
			return width;
		}

		int Screen::getHeight()
		{
			return height;
		}
	}
}