#include <iostream>

#include "FileManager.h"
#include "Shader.h"

namespace tuvey
{
	namespace engine
	{
		Shader::Shader()
		{

		}

		Shader::Shader(std::string vertexFile, std::string fragmentFile)
		{
			loadVertexShader(vertexFile);
			loadFragmentShader(fragmentFile);
			createProgram();
		}

		Shader::~Shader()
		{

		}

		void Shader::loadVertexShader(std::string vertexFile)
		{
			vertexShader_ = FileManager::loadFile(vertexFile);
		}

		void Shader::loadFragmentShader(std::string fragmentFile)
		{
			fragmentShader_ = FileManager::loadFile(fragmentFile);
		}

		GLuint Shader::getProgramId()
		{
			return programId_;
		}

		GLuint Shader::getVertexShaderId()
		{
			return vertexShaderId_;
		}

		GLuint Shader::getFragmentShaderId()
		{
			return fragmentShaderId_;
		}

		void Shader::createProgram()
		{
			//Compiles and attaches the vertex and fragment shader to the created shader program
			const char* vertex = vertexShader_.c_str();
			const char* fragment = fragmentShader_.c_str();

			vertexShaderId_ = glCreateShader(GL_VERTEX_SHADER);
			glShaderSource(vertexShaderId_, 1, &vertex, NULL);
			glCompileShader(vertexShaderId_);
			checkShaderCompiled(vertexShaderId_);

			fragmentShaderId_ = glCreateShader(GL_FRAGMENT_SHADER);
			glShaderSource(fragmentShaderId_, 1, &fragment, NULL);
			glCompileShader(fragmentShaderId_);
			checkShaderCompiled(fragmentShaderId_);
			checkShaderCompiled(fragmentShaderId_);

			programId_ = glCreateProgram();
			glAttachShader(programId_, vertexShaderId_);
			glAttachShader(programId_, fragmentShaderId_);
			glLinkProgram(programId_);
		}

		bool Shader::checkShaderCompiled(GLuint shader)
		{
			//Checks the compile status of the specified shader
			GLint compiled;
			glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
			if (!compiled)
			{
				GLsizei length;
				glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);

				GLchar* log = new GLchar[length + 1];
				glGetShaderInfoLog(shader, length, &length, log);
				std::cerr << "ERROR: Shader compilation failed: " << log << std::endl;
				delete[] log;

				return false;
			}
			return true;
		}
	}
}