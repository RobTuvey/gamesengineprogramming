#include <gtc/type_ptr.hpp>
#include <iostream>

#include "Application.h"
#include "Material.h"
#include "SDL.h"
#include "SDL_image.h"
#include "Shader.h"

namespace tuvey
{
	namespace engine
	{
		Material::Material()
		{
			textureId_ = 0;
		}

		Material::~Material()
		{

		}

		Material::Material(std::string vertexFile, std::string fragmentFile)
		{
			shader_ = shared<Shader>(new Shader(vertexFile, fragmentFile));

			//Initializes the values for the intensities of the light calculations
			specularIntensity_ = 1.5f;
			diffuseIntensity_ = 15.0f;

			setFloat("in_SpecInt", specularIntensity_);
			setFloat("in_DiffInt", diffuseIntensity_);
			refreshAttributeIds();

			textureId_ = 0;
		}

		void Material::refreshAttributeIds()
		{
			//Initialzies the GLSL locations
			positionId_ = glGetAttribLocation(shader_->getProgramId(), "in_Position");
			uvId_ = glGetAttribLocation(shader_->getProgramId(), "in_Uv");
			normalId_ = glGetAttribLocation(shader_->getProgramId(), "in_Normal");
			modelUniformId_ = glGetAttribLocation(shader_->getProgramId(), "in_Model");
		}

		void Material::refreshUniformIds()
		{
			//Updates the uniform variable location ids
			dirtyIds_ = false;

			for (size_t i = 0; i < matrixNames_.size(); i++)
			{
				GLint uniformID = glGetUniformLocation(shader_->getProgramId(), matrixNames_.at(i).c_str());

				if (uniformID == -1) continue;

				matrixIds_[i] = uniformID;
			}

			for (size_t i = 0; i < floatNames_.size(); i++)
			{
				GLuint uniformID = glGetUniformLocation(shader_->getProgramId(), floatNames_.at(i).c_str());

				if (uniformID == -1) continue;

				floatIds_[i] = uniformID;
			}

			for (size_t i = 0; i < vector2Names_.size(); i++)
			{
				GLuint uniformID = glGetUniformLocation(shader_->getProgramId(), vector2Names_.at(i).c_str());

				if (uniformID == -1) continue;

				vector2Ids_[i] = uniformID;
			}
			for (size_t i = 0; i < vector3Names_.size(); i++)
			{
				GLuint uniformID = glGetUniformLocation(shader_->getProgramId(), vector3Names_.at(i).c_str());

				if (uniformID == -1) continue;

				vector3Ids_[i] = uniformID;
			}
			for (size_t i = 0; i < vector4Names_.size(); i++)
			{
				GLint uniformID = glGetUniformLocation(shader_->getProgramId(), vector4Names_.at(i).c_str());

				if (uniformID == -1) continue;

				vector4Ids_[i] = uniformID;
			}
			for (size_t i = 0; i < samplerNames_.size(); i++)
			{
				GLint uniformID = glGetUniformLocation(shader_->getProgramId(), samplerNames_.at(i).c_str());

				if (uniformID == -1) continue;

				samplerIds_[i] = uniformID;
			}
		}

		void Material::insertData()
		{
			//Inserts data into the shader and sets it as the current material
			Application::context_->currentMaterial = shared_from_this();
			glUseProgram(shader_->getProgramId());

			if (dirtyIds_)
				refreshUniformIds();

			for (size_t i = 0; i < matrixNames_.size(); i++)
			{
				glUniformMatrix4fv(matrixIds_[i], 1, GL_FALSE, glm::value_ptr(matrices_[i]));
			}

			for (size_t i = 0; i < vector2Names_.size(); i++)
			{
				glUniform2f(vector2Ids_[i], vector2s_[i].x, vector2s_[i].y);
			}

			for (size_t i = 0; i < floatNames_.size(); i++)
			{
				glUniform1f(floatIds_[i], floats_[i]);
			}

			for (size_t i = 0; i < vector3Names_.size(); i++)
			{
				glUniform3f(vector3Ids_[i], vector3s_[i].x, vector3s_[i].y, vector3s_[i].z);
			}

			for (size_t i = 0; i < vector4Names_.size(); i++)
			{
				glUniform4f(vector4Ids_[i], vector4s_[i].x, vector4s_[i].y, vector4s_[i].z, vector4s_[i].w);
			}

			for (size_t i = 0; i < samplerNames_.size(); i++)
			{
				glUniform1i(samplerIds_[i], samplers_[i]);
			}
		}

		void Material::loadTexture(std::string filename)
		{
			//Creates texture and binds it for rendering
			SDL_Surface* texture = IMG_Load(filename.c_str());

			if (!texture)
				std::cout << IMG_GetError() << std::endl;
			else
			{
				glGenTextures(1, &textureId_);
				glBindTexture(GL_TEXTURE_2D, textureId_);
				glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, texture->w, texture->h, 0, GL_BGR, GL_UNSIGNED_BYTE, texture->pixels);

				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
				glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
				glGenerateMipmap(GL_TEXTURE_2D);
			}
		}

		void Material::setMatrix(std::string name, glm::mat4 value)
		{
			for (size_t i = 0; i < matrixNames_.size(); i++)
			{
				if (matrixNames_.at(i) == name)
				{
					matrices_[i] = value;
					return;
				}
			}

			matrices_.push_back(value);
			matrixIds_.push_back(-1);
			matrixNames_.push_back(name);
			
			dirtyIds_ = true;
		}

		void Material::setFloat(std::string name, float value)
		{
			for (size_t i = 0; i < floatNames_.size(); i++)
			{
				if (floatNames_.at(i) == name)
				{
					floats_[i] = value;
					return;
				}
			}

			floats_.push_back(value);
			floatIds_.push_back(-1);
			floatNames_.push_back(name);

			dirtyIds_ = true;
		}

		void Material::setVector2(std::string name, glm::vec2 value)
		{
			for (size_t i = 0; i < vector2Names_.size(); i++)
			{
				if (vector2Names_.at(i) == name)
				{
					vector2s_[i] = value;
					return;
				}
			}

			vector2s_.push_back(value);
			vector2Ids_.push_back(-1);
			vector2Names_.push_back(name);

			dirtyIds_ = true;
		}

		void Material::setVector3(std::string name, glm::vec3 value)
		{
			for (size_t i = 0; i < vector3Names_.size(); i++)
			{
				if (vector3Names_.at(i) == name)
				{
					vector3s_[i] = value;
					return;
				}
			}

			vector3s_.push_back(value);
			vector3Ids_.push_back(-1);
			vector3Names_.push_back(name);

			dirtyIds_ = true;
		}

		void Material::setVector4(std::string name, glm::vec4 value)
		{
			for (size_t i = 0; i < vector4Names_.size(); i++)
			{
				if (vector4Names_.at(i) == name)
				{
					vector4s_[i] = value;
					return;
				}
			}

			vector4s_.push_back(value);
			vector4Ids_.push_back(-1);
			vector4Names_.push_back(name);

			dirtyIds_ = true;
		}

		void Material::setSampler(std::string name, int value)
		{
			for (size_t i = 0; i < samplerNames_.size(); i++)
			{
				if (samplerNames_.at(i) == name)
				{
					samplers_[i] = value;
					return;
				}
			}

			samplers_.push_back(value);
			samplerIds_.push_back(-1);
			samplerNames_.push_back(name);

			dirtyIds_ = true;
		}

		glm::mat4 Material::getMatrix(std::string name)
		{
			for (size_t i = 0; i < matrixNames_.size(); i++)
			{
				if (matrixNames_.at(i) == name)
					return matrices_.at(i);
			}

			return glm::mat4(0.0f);
		}

		float Material::getFloat(std::string name)
		{
			for (size_t i = 0; floatNames_.size(); i++)
			{
				if (floatNames_.at(i) == name)
					return floats_.at(i);
			}

			return -1;
		}

		glm::vec2 Material::getVector2(std::string name)
		{
			for (size_t i = 0; i < vector2Names_.size(); i++)
			{
				if (vector2Names_.at(i) == name)
					return vector2s_.at(i);
			}

			return glm::vec2(0.0f);
		}

		glm::vec3 Material::getVector3(std::string name)
		{
			for (size_t i = 0; i < vector3Names_.size(); i++)
			{
				if (vector3Names_.at(i) == name)
					return vector3s_.at(i);
			}

			return glm::vec3(0.0f);
		}

		glm::vec4 Material::getVector4(std::string name)
		{
			for (size_t i = 0; i < vector4Names_.size(); i++)
			{
				if (vector4Names_.at(i) == name)
					return vector4s_.at(i);
			}

			return glm::vec4(0.0f);
		}

		int Material::getSampler(std::string name)
		{
			for (size_t i = 0; i < samplerNames_.size(); i++)
			{
				if (samplerNames_.at(i) == name)
					return samplers_.at(i);
			}

			return 0;
		}

		GLint Material::getPositionId()
		{
			return positionId_;
		}

		GLint Material::getUvId()
		{
			return uvId_;
		}

		GLint Material::getNormalId()
		{
			return normalId_;
		}

		GLint Material::getModelUId()
		{
			return modelUniformId_;
		}

		GLint Material::getTextureId()
		{
			return textureId_;
		}

		shared<Shader> Material::getShader()
		{
			return shader_;
		}
	}
}