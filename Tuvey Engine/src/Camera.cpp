#include "Application.h"
#include "Camera.h"
#include "GameObject.h"
#include "Screen.h"

namespace tuvey
{
	namespace engine
	{
		Camera::Camera()
		{
			std::cout << "++ [Camera] Component Added" << std::endl;
		}

		Camera::~Camera()
		{
			std::cout << "-- [Camera] Component Removed" << std::endl;
		}

		glm::mat4 Camera::getProjectionMatrix()
		{
			return projectionMatrix_;
		}

		void Camera::setProjectionMatrix(glm::mat4 matrix)
		{
			projectionMatrix_ = matrix;
		}

		weak<Camera> Camera::getCurrent()
		{
			return Application::context_->currentCamera;
		}

		void Camera::awake()
		{
			Application::context_->currentCamera = shared_from_this();
			projectionMatrix_ = glm::perspective(45.0f, (float)Screen::getWidth() / (float)Screen::getHeight(), 0.1f, 1000.0f);
		}
	}
}