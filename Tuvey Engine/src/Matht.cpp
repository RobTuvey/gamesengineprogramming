#include "Matht.h"

namespace tuvey
{
	namespace engine
	{

		float Math::PI = 3.14159265359f;

		float Math::deg2Rad(float degree)
		{
			return degree * (PI / 180);
		}

		glm::mat4 Math::getTransformation(glm::vec3 position, glm::vec3 rotation, glm::vec3 scale)
		{
			//Creates an identity matrix and applies the position, rotation and scale to create the transformation matrix of the object
			glm::mat4 mat = glm::mat4(1.0f);

			mat = glm::translate(mat, position);

			mat = glm::rotate(mat, rotation.x, glm::vec3(1, 0, 0));
			mat = glm::rotate(mat, rotation.y, glm::vec3(0, 1, 0));
			mat = glm::rotate(mat, rotation.z, glm::vec3(0, 0, 1));

			mat = glm::scale(mat, scale);

			return mat;
		}

		glm::mat3 Math::getRotationMatrix(glm::vec3 rotation)
		{
			//Creates an identity matrix and applies the rotation to create the rotation matrix
			glm::mat4 mat = glm::mat4(1.0f);

			mat = glm::rotate(mat, rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
			mat = glm::rotate(mat, rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
			mat = glm::rotate(mat, rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));

			return glm::mat3(mat);
		}

		double Math::magnitude(glm::vec3 vector)
		{
			return sqrt((vector.x * vector.x) + (vector.y * vector.y) + (vector.z * vector.z));
		}

		double Math::shortestAngle(glm::vec3 a, glm::vec3 b)
		{
			double angleCos = glm::dot(a, b) / magnitude(a) * magnitude(b);
			double angleRad = acos(angleCos);

			return angleRad * (180 / PI);
		}

		float Math::antiClockwiseAngle(glm::vec3 a, glm::vec3 b, glm::vec3 c)
		{
			float shortAngle = shortestAngle(a, b);

			float trip = triple(a, b, c);
			if (trip < 0)
				return 360 - shortAngle;

			return shortAngle;
		}

		float Math::clockwiseAngle(glm::vec3 a, glm::vec3 b, glm::vec3 c)
		{
			float shortAngle = shortestAngle(a, b);

			float trip = triple(a, b, c);
			if (trip > 0)
				return 360 - shortAngle;

			return shortAngle;
		}

		float Math::triple(glm::vec3 a, glm::vec3 b, glm::vec3 c)
		{
			return glm::dot(c, glm::cross(a, b));
		}
	}
}