#include <fstream>
#include <iterator>
#include <sstream>
#include <glm.hpp>

#include "FileManager.h"
#include "Mesh.h"

namespace tuvey
{
	namespace engine
	{
		struct face
		{
			std::vector<int>		vertexIds_;
			std::vector<int>		uvIds_;
			std::vector<int>		normalIds_;
		};

		Mesh* FileManager::loadObj(std::string filename)
		{
			std::string line = "";
			std::ifstream file(filename);
			std::istringstream ss;

			std::vector<glm::vec3> loadedVertices;
			std::vector<glm::vec2> loadedUvs;
			std::vector<glm::vec3> loadedNormals;

			std::vector<face> faces;

			//Reads obj file in line by line extracting data for each vertex, UV, normal and face of the obj

			if (file.is_open())
			{
				while (getline(file, line))
				{
					ss.clear();
					std::string header = line.substr(0, 2);
					if (line.length() > 2) ss.str(line.substr(2));

					if (header == "v ")
					{
						glm::vec3 v;
						ss >> v.x >> v.y >> v.z;
						loadedVertices.push_back(v);
					}
					else if (header == "vt")
					{
						glm::vec2 v;
						ss >> v.x >> v.y;
						loadedUvs.push_back(v);
					}
					else if (header == "vn")
					{
						glm::vec3 v;
						ss >> v.x >> v.y >> v.z;
						loadedNormals.push_back(v);
					}
					else if (header == "f ")
					{
						ss.str(line.substr(2));
						std::istream_iterator<std::string> beg(ss), end;
						std::vector<std::string> fparts(beg, end);
						ss.clear();

						face currentFace;

						for each (std::string fstring in fparts)
						{
							std::string handle;
							ss.str(fstring);

							for (size_t i = 0; getline(ss, handle, '/'); i++)
							{
								switch (i)
								{
								case 0:
									currentFace.vertexIds_.push_back(atoi(handle.c_str()));
									break;
								case 1:
									currentFace.uvIds_.push_back(atoi(handle.c_str()));
									break;
								case 2:
									currentFace.normalIds_.push_back(atoi(handle.c_str()));
									break;
								}
							}

							ss.clear();
						}

						if (currentFace.vertexIds_.size() > 3)
						{
							face faceA;
							
							faceA.vertexIds_.push_back(currentFace.vertexIds_.at(0)); faceA.uvIds_.push_back(currentFace.uvIds_.at(0)); faceA.normalIds_.push_back(currentFace.normalIds_.at(0));
							faceA.vertexIds_.push_back(currentFace.vertexIds_.at(1)); faceA.uvIds_.push_back(currentFace.uvIds_.at(1)); faceA.normalIds_.push_back(currentFace.normalIds_.at(1));
							faceA.vertexIds_.push_back(currentFace.vertexIds_.at(2)); faceA.uvIds_.push_back(currentFace.uvIds_.at(2)); faceA.normalIds_.push_back(currentFace.normalIds_.at(2));

							face faceB;
							faceB.vertexIds_.push_back(currentFace.vertexIds_.at(2)); faceB.uvIds_.push_back(currentFace.uvIds_.at(2)); faceB.normalIds_.push_back(currentFace.normalIds_.at(2));
							faceB.vertexIds_.push_back(currentFace.vertexIds_.at(3)); faceB.uvIds_.push_back(currentFace.uvIds_.at(3)); faceB.normalIds_.push_back(currentFace.normalIds_.at(3));
							faceB.vertexIds_.push_back(currentFace.vertexIds_.at(0)); faceB.uvIds_.push_back(currentFace.uvIds_.at(0)); faceB.normalIds_.push_back(currentFace.normalIds_.at(0));

							faces.push_back(faceA);
							faces.push_back(faceB);
						}
						else if (currentFace.vertexIds_.size() == 3)
						{
							faces.push_back(currentFace);
						}
					}
				}
			}

			std::vector<glm::vec3> vertices;
			std::vector<glm::vec2> uvs;
			std::vector<glm::vec3> normals;

			for (size_t i = 0; i < faces.size(); i++)
			{
				for (size_t j = 0; j < 3; j++)
				{
					vertices.push_back(loadedVertices[faces.at(i).vertexIds_.at(j) - 1]);
					if (loadedUvs.size() > 0) uvs.push_back(loadedUvs[faces.at(i).uvIds_.at(j) - 1]);
					if (loadedNormals.size() >0) normals.push_back(loadedNormals[faces.at(i).normalIds_.at(j) - 1]);
				}
			}

			return new Mesh(vertices, uvs, normals);
		}

		std::string FileManager::loadFile(std::string filename)
		{
			std::string contents = "";
			std::string line = "";
			std::ifstream file(filename);

			if (file.is_open())
			{
				while (getline(file, line))
				{
					contents.append(line + "\n");
				}
			}

			return contents;
		}
	}
}